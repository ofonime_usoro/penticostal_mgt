<?php
	include_once('init_session.php');
	
	include_once('../components/contact.php');
	include_once('../components/location.php');
	include_once('../components/statistics.php');
	include_once('../components/message.php');
	include_once('../components/contact.php');

	$flag = '';
	$state = '';
	$status = array();
	if(isset($_POST['txtSender']))
	{ 
		$recipients = str_replace("\n", ",", strip_tags($_POST['txtRecipients']));
		$message = new Message();
		$message->set_channel_id(2);
		$zip_code = substr($_POST['downCountry'], 1);
		$message->set_zip_code($zip_code);
		$message->set_other_phones($recipients);
		$message->set_sender(strip_tags($_POST['txtSender']));
		$message->set_message($_POST['txtMessage']);
		if(isset($_POST['downContactGroup']))
			$message->set_contact_ids(implode(',',$_POST['downContactGroup']));
		$message->set_action(isset($_POST['btnSend'])?0:1);
		
		$state = isset($_POST['btnSend'])?'sent':'saved';
		if(isset($_POST['d']))
			$message->set_delivery(strip_tags($_POST['d']));
		$message->process();// send or save as message draft
		
		$flag = $message->get_flag();
		
		if($flag == 0)
		{
			$status['style'] = 'alert-error';
			$status['title'] = 'Error';
			$status['message'] = 'Unable to send your message now. Please try again later!';
		}
		else if($flag == 1)
		{
			$status['style'] = 'alert-success';
			$status['title'] = 'Success';
			$status['message'] = 'Your message was sent successfully and '.$message->units_charged().' units charged!';
		}
		else if($flag == 2)
		{
			$status['style'] = 'alert-success';
			$status['title'] = 'Success';
			$status['message'] = 'Your message was saved successfully!';
		}
		else if($flag == 3)
		{
			$status['style'] = 'alert-error';
			$status['title'] = 'Error';
			$status['message'] = 'Your have insufficient credit units!';
		}
		else if($flag == 4)
		{
			$status['style'] = 'alert-error';
			$status['title'] = 'Error';
			$status['message'] = 'You have not specified recipients of your message!';
		}
		else if($flag == 5)
		{
			$status['style'] = 'alert-success';
			$status['title'] = 'Success';
			$status['message'] = 'Your message has been scheduled for later delivery!';
		}
		
		if(!empty($_POST['txtContactGroup']) && !empty($_POST['txtRecipients']))
		{
			$contact = new ContactInfo();
			$contact->set_name(strip_tags($_POST['txtContactGroup']));
			
			if($cgi = $contact->insert())
			{
				$contact->set_id($cgi);
				$contact->set_phone_nos($recipients);
				$contact->add_contact_nos();
				$status['message'] .= '<br/>Your contact group "'.$_POST['txtContactGroup'].'" was created successfully and your phone numbers added to the group!';
			}
			else
			{
				$status['message'] .= '<br/>The contact group "'.$_POST['txtContactGroup'].'" you want to create already exit. Please go to Contact Group menu to add more phone numbers to existing group!';	
			}
			
		}
	}
	
	$contact = new ContactInfo();
	$contact_groups = json_decode($contact->fetch_user_contacts(''), true);
	
	$location = new Location();
  	$location->set_type('c');
		
  	$countries = json_decode($location->fetch(), true);
	
	header('Content-Type: text/html');

	$statistics = new Statistics();

	$current_units = json_decode($statistics->current_units(), true);
	
	$page_title = 'Compose Bulk SMS';
	include('header.php');
	include('menu.php');
	
	display_menu(2, 0);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-envelope"></i> Compose Bulk SMS</h1>
                        <h4>Compose, Send, Save, Schedule, Send to Contact Group</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="active">
                        	<i class="icon-home"></i> Home 
                            <span class="divider">
								<i class="icon-angle-right"></i>
							</span>
						</li>
                        <li>
                        	 Compose Bulk SMS
                        </li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
				<?php if(count($status)){?>
				<div class="row-fluid">
                    <div class="span12">
                    	<div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>
                 </div>
                 <?php }?>
                <!-- BEGIN Main Content -->
                
				<div class="row-fluid">
                	<div class="span8">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-edit"></i> Compose Bulk SMS (Balance: <?php echo $current_units['cb']?> units)</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" method="post" class="form-horizontal">
                                   <div class="control-group">
                                      <label class="control-label">Sender ID</label>
                                      <div class="controls">
                                         <input type="text" required maxlength="11" name="txtSender" placeholder="Sender ID" class="input-block-level" />
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label">Recipients Country</label>
                                      <div class="controls">
                                         <select class="input-block-level" required name="downCountry">
											<?php
                                                $selected = isset($_POST['downCountry']) ? $_POST['downCountry'] : $_SESSION['zip_code'];
                                                foreach($countries as $country)
                                                {
                                                    echo '<option value="+'.$country['c'].'" '.(($selected == $country['c']) ? 'selected' : '').'>'.$country['n'].' - [+'.$country['c'].']</option>';
                                                }
                                            ?>
                                        </select>
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label">Contact Groups</label>
                                      <div class="controls">
                                         <select name="downContactGroup[]" data-placeholder="If you want to send to group, choose your contact group" class="chosen span12" multiple="multiple">
                                         
                                         	<?php
                                            	foreach($contact_groups as $cg)
												{
													echo '<option value="'.$cg['i'].'">'.$cg['n'].' ['.$cg['t'].']</option>';
												}
											?>
                                         </select>
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label" rows="5">Recipients</label>
                                      <div class="controls">
                                         <textarea class="input-block-level" name="txtRecipients" id="txtRecipients" placeholder="Separate multiple numbers with comma or type each number in a new line"></textarea>
                                      </div>
                                   </div>
                                   <div class="control-group div-save-contact">
                                      <label class="control-label" rows="5">Save Numbers to Contact Group</label>
                                      <div class="controls">
                                         <input type="checkbox" name="cbContactGroup" id="cbContactGroup"/> 
                                         <input type="text" readonly required name="txtContactGroup" placeholder="Enter Contact Group Name" id="txtContactGroup" class="input-xlarge" />
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label">Message</label>
                                      <div class="controls">
                                         <textarea required name="txtMessage" id="txtMessage" placeholder="Type your message here" class="input-block-level"></textarea>
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label"></label>
                                      <div class="controls div-page-count">
                                         
                                      </div>
                                   </div>
                                   <div class="form-actions">
                                      <button type="submit" name="btnSend" class="btn btn-primary"><i class="icon-envelope"> </i> Send</button>
                                      <button type="submit" name="btnSave" class="btn btn-primary"><i class="icon-save"> </i> Save</button>
                                      <button type="button" name="btnSchedule" class="btn btn-primary"><i class="icon-time"> </i> Schedule</button>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-info"></i> Hints</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                    	<p>Dear esteemed <?PHP echo $_SESSION['user_name'];?>,</p>
                                        
                                        The following words have been blocked on our Server:                                
                                        <p class="alert-error">congrats,congrat,congratulations, akpabio,promo,promotion,yello, won, win,
                                        police alert,pr0mo w1n,prom0, pr0m0, wow, promotional, yellow, sex,
                                        sexy,samsung, porn, nude, bank, liyel and prize</p>
                                        
                                        <p>Messages with these words as part of its content are considered as spam
                                        and do not get delivered by the network operators. In order to guard
                                        against loss of sms units from your end, we have put in place a filter to
                                        block such words.</p>
                                        Also, avoid the following in your Sender ID
                                       <p class="alert-error">
                                        1, 2, 3,4, 5, 6, 7, 8, 9, &, _, -, %, $, #, @, !, +, =,
                                        FBI,CIA,Interpol,Interp0l,1nterp0l, Facebook, samsung, orange, sony,
                                        Nokia, tecno, Airtel, Etisalat,orange, <br/>
                                        Sender IDs with these words and figures will be blocked by our system.</p>
                                        
                                        <p>If you require further information and assistance, please do not hesitate
                                        to contact our support.<br/>
                                        Thanks</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- END Main Content -->
                <?php include('footer.php');?>

<script>
	$(function(){
		$('#txtRecipients').keyup(function(e){
			e.preventDefault();
			if($.trim($(this).val()))
			{
				$('.div-save-contact').show();
				isSaveChecked();
			}
			else
			{
				$('.div-save-contact').hide();
			}
		});
		
		$('#txtMessage').keyup(function(e){
			e.preventDefault();
			
			if($(this).val())
			{
				var MAX_SINGLE = 160;
				var MAX_MULTIPLE = 151;
				var msg = $(this).val();
				var pageLen = msg.length <= MAX_SINGLE ? MAX_SINGLE : MAX_MULTIPLE;
				var pages = Math.ceil(msg.length/pageLen);
				
				$('.div-page-count').show();
				$('.div-page-count').html("Pages: "+pages+" (Maximum Charaters: "+(pageLen*pages)+"),   Characters left: "+(pageLen*pages-msg.length)+", Typed Characters: "+msg.length);
			}
			else
			{
				$('.div-page-count').hide();
			}			
		});
		
		$('#cbContactGroup').change(function(e){
			isSaveChecked();
		});
		
		isSaveChecked = function()
		{
			if($('#cbContactGroup').is(":checked"))
			{
				$('#txtContactGroup').show();
				$('#txtContactGroup').attr('readonly', false);
			}
			else
			{
				$('#txtContactGroup').hide();
				$('#txtContactGroup').attr('readonly', true);
				$('#txtContactGroup').val('');
			}
		}
		$('#txtContactGroup').hide();
		$('.div-save-contact').hide();
		$('.div-page-count').hide();
	
	});
</script>
