<?php
    session_start();
    require_once("data/other_income_type.php");
    require_once("data/other_income_type_dal.php"); 

    $status = array();
    if(!isset($_SESSION['user_id'])){
        header("Location: index.php");
        exit();
    }

    $other_income_type = null;

    if(isset($_POST['submit'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName']) && isset($_POST['txtDate']) && !empty($_POST['txtDate'])){
            $name = strip_tags($_POST['txtName']);
            $description = strip_tags($_POST['txtDescription']);
            $date = strip_tags($_POST['txtDate']);
            

            $other_income_type = new OtherIncomeType();
            $other_income_type->set_name($name);
            $other_income_type->set_description($description);
            $other_income_type->set_date_created($date);
            $other_income_type->set_date_updated(date('Y-m-d H:i:s'));
            
            $other_income_type_dal = new OtherIncomeTypeDAL($other_income_type);


            if(isset($_POST['submit'])){
                $flag = $other_income_type_dal->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Income Type was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }
            }else {
                $other_income_type->set_id($_GET['other_income_type_id']);
                $flag = $other_income_type_dal->update();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Income Type was updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }
            }
        }
    }

    if(isset($_GET['other_income_type_id'])){
        $other_income_type = OtherIncomeTypeDAL::fetch($_GET['other_income_type_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(7, 0);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Other Income Type</h1>
                        <h4>Form Other Income Type</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Other Income Type</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Other Income Type Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="txtName">Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtName" id="txtName" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" value="<?php echo ($other_income_type != null) ? $other_income_type->get_name() : ''; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" placeholder="Enter Description"><?php echo ($other_income_type != null) ? $other_income_type->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="YYYY-MM-DD" name="txtDate" id="txtDate" data-rule-date="true" data-rule-required="true" value="<?php echo ($other_income_type != null) ? $other_income_type->get_date() : date('d/m/Y', time()); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['edit_member'])){ echo 'Update'; }else { echo 'Register'; } ?>' name="<?php if(isset($_GET['edit_member'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>