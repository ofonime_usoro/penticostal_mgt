<?php
    session_start(); 
    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }
    
    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(0, 0);

?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Dashboard</h1>
                        <!--h4>Overview, stats, chart and more</h4-->
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="active"><i class="icon-home"></i> Home</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="lead" style="width:100%; height:4opx; text-align:center">
                            <h1>Welcome <?php  echo $_SESSION['username']; ?><br/> To<br/>
                            PENTICOSTAL ASSEMBLIES MANAGEMENT SOFTWARE</h1>
                        </div>
                    </div>
                 </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>
                
                