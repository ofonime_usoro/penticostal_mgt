<?php
    session_start(); 

    require_once("data/offering_type.php");
    require_once("data/offering_type_dal.php");

    if( ! isset( $_SESSION['user_id'] ) ){
        header("Location: login.php");
        exit();
    }



    $status = array();
    $offering_type = null;

    if(isset($_POST["submit"]) || isset($_POST['update'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName']) && isset($_POST['txtDescription']) && !empty($_POST['txtDescription'])){
            $offering_type_name = strip_tags($_POST['txtName']);
            $offering_type_description = strip_tags($_POST['txtDescription']);

            $offering_type = new OfferingType();
            $offering_type->set_name($offering_type_name);
            $offering_type->set_description($offering_type_description);

            $offering_type_model = new OfferingTypeDAL($offering_type);

            if(isset($_POST['submit'])){

                $flag = $offering_type_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Offering Type was added successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering Offering Type. Try again';
                }

            }else {
                $offering_type->set_id($_GET['offering_type_id']);
                $flag = $offering_type_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Offering Type updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating Offering Type. Try again';
                }  
            }
        }
    }


    if(isset($_GET['offering_type_id'])){
        $offering_type = OfferingTypeDAL::fetch($_GET['offering_type_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(3, 0);

?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Offering Type Form</h1>
                        <h4>Use this form to register new Offering Type</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Offering Type Form</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <!--form action="#" class="form-horizontal" id="validation-form" method="get"-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Offering Type Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label for="txtName" class="control-label">Name</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtName" id="txtName" data-rule-maxlength="50" data-rule-required="true" 
                                            value="<?php echo ($offering_type != null) ? $offering_type->get_name() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" id="txtDescription" name="txtDescription" placeholder="Enter Description"><?php echo ($offering_type != null) ? $offering_type->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>     
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['offering_type_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['offering_type_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>