<?php
    session_start();
    require_once("data/expenditure.php");
    require_once("data/expenditure_dal.php");
    require_once("data/expenditure_account_dal.php");


    if( ! isset( $_SESSION['user_id'] ) ){
        header("Location: login.php");
        exit();
    }

    $status = array();
    $expenditure = null;

    
    $expenditure_account_dal = ExpenditureAccountDAL::fetch_all();

    if(isset($_POST['submit']) || isset($_POST['update'])){
        if(isset($_POST['txtAmount']) && !empty($_POST['txtAmount']) && isset($_POST['txtDate']) && !empty($_POST['txtDate']) && isset($_POST["downExpenditureAccountDAL"]) && !empty($_POST["downExpenditureAccountDAL"])){


            $expenditure_amount = strip_tags($_POST['txtAmount']);
            $expenditure_date = strip_tags($_POST['txtDate']);
            $expenditure_description = strip_tags($_POST['txtDescription']);
            $expenditure_id = strip_tags($_POST['downExpenditureAccountDAL']);

            $expenditure = new Expenditure();
            $expenditure->set_account_id($expenditure_id);
            $expenditure->set_amount($expenditure_amount);
            $expenditure->set_date($expenditure_date);
            $expenditure->set_description($expenditure_description);
            $expenditure->set_user_id($_SESSION['user_id']);

            $expenditure_model = new ExpenditureDAL($expenditure);

            if(isset($_POST['submit'])){

                $flag = $expenditure_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Expenditure was added successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering Expenditure. Try again';
                }

            }else {
                $expenditure->set_id($_GET['expenditure_id']);
                $flag = $expenditure_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Expenditure updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating expenditure. Try again';
                }  
            }     
        }
    }

    if(isset($_GET['expenditure_id'])){
        $expenditure = ExpenditureDAL::fetch($_GET['expenditure_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(4, 1);

?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Expenditure</h1>
                        <h4>Expenditure</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Expenditure</li>
                    </ul>
                </div>
                
                <!-- END Breadcrumb -->
                <?php if(count($status)){?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>
                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i> Expenditure</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal"  method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="type">Account Type</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name="downExpenditureAccountDAL">
                                                    <?php 
                                                        if(isset($expenditure_account_dal)){
                                                            foreach($expenditure_account_dal as $value) { ?>
                                                                <option value="<?= $value->id; ?>"><?= $value->name; ?></option>
                                                            <?php }
                                                        }
                                                    ?>
                                                    
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" 
                                            value="<?php echo ($expenditure != null) ? $expenditure->get_amount() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDate" id="txtDate" value="<?php echo ($expenditure != null) ? $expenditure->get_date() : date('d/m/Y', time()) ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                         </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" id="txtDescription" placeholder="Enter Description"><?php echo ($expenditure != null) ? $expenditure->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>   

                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['expenditure_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                                        name="<?php if(isset($_GET['expenditure_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>