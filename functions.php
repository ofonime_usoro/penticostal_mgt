<?php 
	require_once("data/offering_type_dal.php");
	require_once("data/core.php");

	function get_offering_type_dropdown()
	{
		$query = "SELECT id, name FROM offering_type";
		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);
			$datas = $result->fetchAll(PDO::FETCH_OBJ);
			echo "<select name='downOfferingType'>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}

			echo "</select>";

		}catch(PDOException $e){
			echo $e->getMessage();
		}
			
	}


	function get_offering_type_name_by_id($id)
	{
		$query = "SELECT name FROM offering_type WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}


	function get_all_state_dropdown()
	{
		$query = "SELECT * FROM state";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}



	function get_all_lga_dropdown()
	{
		$query = "SELECT * FROM lgas";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downLgaId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";

		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_group_dropdown()
	{
		$query = "SELECT * FROM `group`";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downGroupId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";
		}catch(PDOException $e){
			echo $e->getMessage();
		}

	}

	function get_all_department_dropdown()
	{
		$query = "SELECT * FROM department";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downDepartmentId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";
		}catch(PDOException $e){
			echo $e->getMessage();
		}

	}

	function get_all_fellowship_dropdown()
	{
		$query = "SELECT * FROM fellowship";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downFellowshipId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";
		}catch(PDOException $e){
			echo $e->getMessage();
		}

	}

	function get_all_assembly_dropdown()
	{
		$query = "SELECT * FROM assembly";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downAssemblyId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";
		}catch(PDOException $e){
			echo $e->getMessage();
		}

	}

	function get_all_income_type_dropdown(){
		$query = "SELECT * FROM other_income_type";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			echo "<select name='downIncomeTypeId'>";
			echo "<option value='0'>-- Please select --</option>";
			foreach($datas as $data){
				echo "<option value=".$data->id.">".$data->name." </option>";
			}
			echo "</select>";
		}catch(PDOException $e){
			echo $e->getMessage();
		}		
	}

	function get_all_users(){
		$query = "SELECT id, username, full_name, email, email, address, phone_no FROM user";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}


	function get_all_members(){
		$query = "SELECT * FROM member";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_assemblies(){
		$query = "SELECT * FROM assembly";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_departments(){
		$query = "SELECT * FROM department";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_offering_types(){
		$query = "SELECT * FROM offering_type";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_expenditure_account(){
		$query = "SELECT * FROM expenditure_account";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_other_income_account(){
		$query = "SELECT * FROM other_income_account";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_other_income_types() {
		$query = "SELECT * FROM other_income_type";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_other_incomes(){
		$query = "SELECT * FROM other_income";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_expenditures(){
		$query = "SELECT * FROM expenditure";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_fund_raising(){
		$query = "SELECT * FROM fund_raising";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function  get_all_redeemed_pledges(){
		$query = "SELECT * FROM redeemed_pledge";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_fund_raising_type_name_by_id($id){
		$query = "SELECT name FROM fund_raising_type WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_state_by_id($id){
		$query = "SELECT name FROM state WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_lga_by_id($id){
		$query = "SELECT name FROM lgas WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_group_by_id($id){
		$query = "SELECT name FROM `group` WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_department_by_id($id){
		$query = "SELECT name FROM department WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_fellowship_by_id($id){
		$query = "SELECT name FROM fellowship WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_assembly_by_id($id){
		$query = "SELECT name FROM assembly WHERE id = :id";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->prepare($query);
			$stmt = $result->execute(array(
				"id" => intval($id)
			));

			$data = $result->fetch(PDO::FETCH_OBJ);
			return $data->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}



	function get_all_offerings(){
		$query = "SELECT * FROM offering";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_tithes(){
		$query = "SELECT * FROM tithe";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}



	function get_expenditure_name_by_id($id){
		$query = "SELECT * FROM expenditure_account WHERE id = :uid";

		try {
			$core = Core::getInstance();
			$stmt = $core->dbh->prepare($query);

			$stmt->execute(array(
				"uid" => intval($id)
			));
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			return $result->name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}



	function get_user_username_by_id($id){
		$query = "SELECT * FROM `user` WHERE id = :uid";

		try {
			$core = Core::getInstance();
			$stmt = $core->dbh->prepare($query);

			$stmt->execute(array(
				"uid" => intval($id)
			));
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			return $result->username;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_member_fullname($id){
		$query = "SELECT first_name, middle_name, last_name FROM `member` WHERE id = :uid";

		try {
			$core = Core::getInstance();
			$stmt = $core->dbh->prepare($query);

			$stmt->execute(array(
				"uid" => intval($id)
			));
			$result = $stmt->fetch(PDO::FETCH_OBJ);
			$full_name = $result->last_name . ", ".$result->first_name ." ".$result->middle_name;

			return $full_name;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}


	function get_all_fellowships(){
		$query = "SELECT * FROM fellowship";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_fund_raising_type(){
		$query = "SELECT * FROM fund_raising_type";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	function get_all_groups(){
		$query = "SELECT * FROM `group`";

		try {
			$core = Core::getInstance();
			$result = $core->dbh->query($query);

			$datas = $result->fetchAll(PDO::FETCH_OBJ);

			return $datas;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}	
