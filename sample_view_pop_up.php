<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>CooperativePro :: View Customer</title>        <meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">         <!--base css styles-->
        <link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/normalize/normalize.css">

        <!--page specific css styles-->
        <link rel="stylesheet" href="assets/prettyPhoto/css/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        
        <link rel="stylesheet" type="text/css" href="assets/data-tables/DT_bootstrap.css" />
        
        <!--flaty css styles-->
        <link rel="stylesheet" href="css/flaty.css">
        <link rel="stylesheet" href="css/flaty-responsive.css">
        
        <link rel="shortcut icon" href="img/favicon.html">
        
        <script src="assets/modernizr/modernizr-2.6.2.min.js"></script>
        
    	<!--basic scripts-->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="assets/jquery/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="assets/bootstrap/bootstrap.min.js"></script>
        <script src="assets/nicescroll/jquery.nicescroll.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <!-- BEGIN Brand -->
                    <a href="#" class="brand">
                    	<i class="icon-globe"></i>
                    	CooperativePro :: GV Multi-Purpose Co-operative Society Limited
                    </a>
                    <!-- END Brand -->
                </div><!--/.container-fluid-->
            </div><!--/.navbar-inner-->
        </div>
        <!-- END Navbar -->
        
        <!-- BEGIN Container -->
        <div class="container-fluid" id="main-container" style="margin-left:-45px">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="nav-collapse sidebar-collapsed">
            </div>
            <!-- END Sidebar -->
                
      			<!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Page Title -->

    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-reorder"></i> Customer Information</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <div class="row-fluid">
                                <div class="span4 controls">
                               		Account No: 
                                </div>
								<div class="span8">
                                	2015010012                                </div>
                            </div>                            
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4">
                               		First Name: 
                                </div>
								<div class="span8 controls">
                                	RITA                                </div>
                            </div> 
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Middle Name: 
                                </div>
								<div class="span8">
                                	NKAN                                </div>
                            </div> 
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Last Name: 
                                </div>
								<div class="span8">
                                	ELIJAH                                </div>
                            </div> 
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Gender: 
                                </div>
								<div class="span8">
                                	Female                                </div>
                            </div> 
                       </div>
                       <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Date of Birth: 
                                </div>
								<div class="span8">
                                	01/02/2015                                </div>
                            </div> 
                       </div>
                       <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Customer Type: 
                                </div>
								<div class="span8">
                                	Member                                </div>
                            </div> 
                       </div>
                       <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Phone No: 
                                </div>
								<div class="span8">
                                	                                </div>
                            </div> 
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Mobile No: 
                                </div>
								<div class="span8">
                                	                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Email: 
                                </div>
								<div class="span8">
                                	                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Address: 
                                </div>
								<div class="span8">
                                	CRUTECH SECURITY                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Account Opening Date: 
                                </div>
								<div class="span8">
                                	01/02/2015                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Marketer: 
                                </div>
								<div class="span8">
                                	Clara Ojong Ereh                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Marital Status: 
                                </div>
								<div class="span8">
                                	Married                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span4 controls">
                               		Religion: 
                                </div>
								<div class="span8">
                                	Christian                                </div>
                            </div>
                        </div>
                     </div>
                        
                    <div class="span6">
                    <div class="box">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                    <ul class="gallery thumbnail">
                                                                        <li>
                                                                            <a title="Customer passport" rel="prettyPhoto" href="/public/images/no_image.gif">
                                                                                <div style="max-width: 200px; max-height: 150px; line-height: 20px;">
                                                                                    <img alt="" src="/public/images/no_image.gif">
                                                                                    <i></i>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                <div class="box">
                                                                    <ul class="gallery thumbnail">
                                                                        <li>
                                                                            <a title="Customer signature" rel="prettyPhoto" href="/public/images/no_image.gif">
                                                                                <div style="max-width: 200px; max-height: 100px; line-height: 20px;">
                                                                                    <img alt="" src="/public/images/no_image.gif">
                                                                                    <i></i>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                    
                   </div>
                   </div>
                   <div class="row-fluid">
                       <div class="span12">
                        <hr>
                        
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Home Town: 
                                </div>
								<div class="span10">
                                	UGEP                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Country: 
                                </div>
								<div class="span10">
                                	Nigeria                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		State: 
                                </div>
								<div class="span10">
                                	Cross River                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		LGA: 
                                </div>
								<div class="span10">
                                	Yakurr                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Occupation: 
                                </div>
								<div class="span10">
                                	SECURITY PERSONEL                                </div>
                            </div>
                        </div>
                        <hr>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-magenta">
                <div class="box-title">
                    <h3><i class="icon-reorder"></i> Next of Kin Details</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                    </div>
                </div>
                <div class="box-content">
                  <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		First Name: 
                                </div>
								<div class="span10">
                                	RITA                                 </div>
                            </div>
                  </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Middle Name: 
                                </div>
								<div class="span10">
                                	NKAN                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Last Name: 
                                </div>
								<div class="span10">
                                	ELIJAH                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Gender: 
                                </div>
								<div class="span10">
                                	Female                                </div>
                            </div>
                       </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Date of Birth: 
                                </div>
								<div class="span10">
                                	01/02/2015                                </div>
                            </div>
                        </div>
                       
                        <hr>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Phone No: 
                                </div>
								<div class="span10">
                                	                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Mobile No: 
                                </div>
								<div class="span10">
                                	                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Email: 
                                </div>
								<div class="span10">
                                	                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Address: 
                                </div>
								<div class="span10">
                                	CRUTECH SECURITY                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Marital Status: 
                                </div>
								<div class="span10">
                                	Single                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Religion: 
                                </div>
								<div class="span10">
                                	Christian                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        
                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Home Town: 
                                </div>
								<div class="span10">
                                	UGEP                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		Country: 
                                </div>
								<div class="span10">
                                	Nigeria                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		State: 
                                </div>
								<div class="span10">
                                	Cross River                                </div>
                            </div>
                        </div>

                  		<div class="control-group">
                        	<div class="row-fluid">
                                <div class="span2 controls">
                               		LGA: 
                                </div>
								<div class="span10">
                                	Yakurr                                </div>
                            </div>
                        </div>
              </div>
            </div>
        </div>
    </div>
<!-- END Main Content -->                
                <!--<footer>
                    <p>2016 &copy; Chibex Technologies.</p>
                </footer>-->

                <a id="btn-scrollup" class="btn btn-circle btn-large" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->
        
        <!--page specific plugin scripts-->
        <script src="/public/assets/prettyPhoto/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="/public/assets/chosen-bootstrap/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script type="text/javascript" src="/public/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="/public/assets/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-daterangepicker/date.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
        <script type="text/javascript" src="/public/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
        <script type="text/javascript" src="/public/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript" src="/public/assets/ckeditor/ckeditor.js"></script> 
        
        <!--page specific plugin scripts-->
        <script type="text/javascript" src="/public/assets/jquery-validation/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="/public/assets/jquery-validation/dist/additional-methods.min.js"></script>
     
         <!--flaty scripts-->
        <script src="/public/js/flaty.js"></script>
   </body>
</html>