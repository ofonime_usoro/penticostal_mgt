<?php 
    
    header('Content-Type: text/html');
    $page_title = 'View Fund Raising';
    include('header.php');
    include('menu.php');
    include('utilities/utilities.php');
    include('data/fund_raising_dal.php');
    $all_funds = FundRaisingDAL::fetch_all();

    $count = 1;
    display_menu(5, 5);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Fund Raising</h1>
                        <h4>Fund Raising management</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Fund Raising Information</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i> All Funds</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right clearfix">
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Add new record" href="#"><i class="icon-plus"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="icon-print"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Refresh" href="#"><i class="icon-repeat"></i></a>
                                    </div>
                                    
                                </div>
                                <div class="clearfix"></div>
<table class="table table-advance" id="table1">
    <thead>
        <tr>
            <th>S/No</th>
            <th>Fund Raising Id</th>
            <th>Amount</th>
            <th>Amount Redeemed</th>
            <th>Name</th>
            <th>User Name</th>
            <th>Phone Number</th>
            <th>Address</th>
            <th>Date</th>
            <th>Created</th>
            <th style="width:100px">Action</th>
        </tr>
    </thead>
    <tbody>

        <?php foreach($all_funds as $funds){?>

            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $funds->fund_raising_id; ?></td>
                <td><?php echo $funds->amount; ?></td>
                <td><?php echo $funds->amount_redeemed; ?></td>
                <td><?php echo $funds->name; ?></td>
                <td><?php echo get_user_name($funds->user_id); ?></td>
                <td><?php echo $funds->phone_no; ?></td>
                <td><?php echo $funds->address; ?></td>
                <td><span class="label label-success"><?php echo $funds->date ?></span></td>
                <td><?php echo $funds->created; ?></td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small show-tooltip view" title="View" href="javascript.void(0)" data-view-url="fund-raising-info.php?fund_raising_id=<?php echo $funds->id; ?>"><i class="icon-zoom-in"></i></a>
                        <a class="btn btn-small show-tooltip" title="Edit" href="<?php echo "fund-raising.php?fund_raising_id=$funds->id";?>"><i class="icon-edit"></i></a>
                        <a id='removeTithe' class="btn btn-small btn-danger show-tooltip a-delete" title="Delete" href="javascript.void(0)" data-delete-id="<?php echo $funds->id; ?>"><i class="icon-trash"></i></a>
                    </div>
                </td>
            </tr>

        <?php } ?>
    </tbody>
</table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                
                <?php include('footer.php');?>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('a.view').click(function(e){
            e.preventDefault();
            open($(this).data('view-url'),'ViewMember', 'menubar=no, scrollbars=yes, resizable=yes,width=950');
        });


        $(".a-delete").click(function(e){
            e.preventDefault();
            
            if(confirm("Are You sure you want to remove this Fund Raising ?")){
                var fund_raising_id = $(this).data('delete-id');
                
                var postForm = {
                    "remove_fund_raising" : fund_raising_id
                };

                //Make an ajax call that deletes the specified member
                $.ajax({
                    type : 'POST',
                    url : 'request.php',
                    data : postForm,
                    dataType: 'json', 
                    success : function(data){
                        if(data.status === true){
                            location.reload();
                        }else if(data.status === false){
                            alert("Could not remove the member");
                        }
                    }

                });
            }
        });
    });
</script>