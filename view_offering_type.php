<?php 
    
    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    include('data/offering_type_dal.php');
    $all_offering_types = OfferingTypeDAL::fetch_all();
    $count = 1;

    display_menu(0, 0);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Offering Types</h1>
                        <h4>Offering Type Management</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Offering Type Information</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i> All Offering Types</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right clearfix">

                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Add new record" href="#"><i class="icon-plus"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="icon-print"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Refresh" href="#"><i class="icon-repeat"></i></a>
                                    </div>
                                    
                                </div>
                                <div class="clearfix"></div>
<table class="table table-advance" id="table1">
    <thead>
        <tr>
            <th>S/No</th>
            <th>Name</th>
            <th>Description</th>
            <th style="width:100px">Action</th>
        </tr>
    </thead>
    <tbody>

        <?php foreach($all_offering_types as $offering_types){?>

            <tr>

                <td><?php echo $count++; ?></td>
                <td><?php echo $offering_types->name; ?></td>
                <td><?php echo $offering_types->description; ?></td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small show-tooltip view" title="View" href="javascript.void(0)" data-view-url="offering-type-info.php?offering_type_id=<?php echo $offering_types->id; ?>"><i class="icon-zoom-in"></i></a>
                        <a class="btn btn-small show-tooltip" title="Edit" href="<?php echo "offering-type.php?offering_type_id=$offering_types->id" ?>"><i class="icon-edit"></i></a>
                        <a class="btn btn-small btn-danger show-tooltip a-delete" data-delete-id="<?php echo $offering_types->id; ?>" title="Delete" href="javascript.void(0)"><i class="icon-trash"></i></a>
                    </div>
                </td>
            </tr>

        <?php } ?>
    </tbody>
</table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                
                <?php include('footer.php');?>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('a.view').click(function(e){
            e.preventDefault();
            open($(this).data('view-url'),'ViewMember', 'menubar=no, scrollbars=yes, resizable=yes,width=950');
        });


        $(".a-delete").click(function(e){
            e.preventDefault();
            
            if(confirm("Are You sure you want to remove this Tithe?")){
                var offering_type_id = $(this).data('delete-id');
                
                var postForm = {
                    "remove_offering_type" : offering_type_id
                };

                //Make an ajax call that deletes the specified member
                $.ajax({
                    type : 'POST',
                    url : 'request.php',
                    data : postForm,
                    dataType: 'json', 
                    success : function(data){
                        if(data.status === true){
                            location.reload();
                        }else if(data.status === false){
                            alert("Could not remove the member");
                        }
                    }

                });
            }
        });
    });
</script>