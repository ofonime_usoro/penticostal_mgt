<?php 

	class User {
		private $id;
		private $username;
		private $email;
		private $password;
		private $full_name;
		private $address;
		private $phone_no;
		private $is_admin;
		private $created;
		private $updated;
		

		public function get_email()
		{
			return $this->email;
		}

		public function set_email($email)
		{
			$this->email = $email;
		}
		public function get_address()
		{
			return $this->address;
		}

		public function set_address($address)
		{
			$this->address = $address;
		}




		public function get_full_name()
		{
			return $this->full_name;
		}

		public function set_full_name($full_name)
		{
			$this->full_name = $full_name;
		}

		public function get_id()
		{
			return $this->id;
		}

		public function set_id($id)
		{
			$this->id = $id;
		}

		public function get_username()
		{
			return $this->username;
		}

		public function set_username($username)
		{
			$this->username = $username;
		}

		public function get_password()
		{
			return $this->password;
		}

		public function set_password($password)
		{
			$this->password = $password;
		}

		public function get_date_created()
		{
			return $this->created;
		}

		public function set_date_created($created)
		{
			$this->created= $created;
		}

		public function get_date_updated()
		{
			return $this->updated;
		}

		public function set_date_updated($date_updated)
		{
			$this->updated = $date_updated;
		}

		public function get_is_admin()
		{
			return $this->is_admin;
		}

		public function set_is_admin($admin_status)
		{
			$this->is_admin = $admin_status;
		}


		public function get_phone_no()
		{
			return $this->phone_no;
		}

		public function set_phone_no($phone_no)
		{
			$this->phone_no = $phone_no;
		}
	}