<?php 
	require_once __DIR__."/marital_status.php";
	require_once __DIR__."/core.php";
	
	class MaritalStatusDAL {
		private $marital_status;

		public function __construct($marital_status){
			$this->marital_status = $marital_status;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO marital_status SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->marital_status->get_name(),
					"description" => $this->marital_status->get_description()
				));
				return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public function fetch($id)
		{
			$query = "SELECT name, description FROM marital_status WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array("id" => intval($id)));
				if($stmt){
					//get the data if the params are bound successfully

					$data = $stmt->fetch(PDO::FETCH_OBJ);
					$marital_status = new MaritalStatus();
					$id = $marital_status->set_id($data->id);
					$name = $marital_status->set_name($data->name);
					$description = $marital_status->set_description($data->description);

					return $marital_status;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all()
		{
			$query = "SELECT * FROM marital_status";
		

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update($id, $name, $description = null){
			$query = "UPDATE marital_status SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array("id" => intval($id), "name" => $name, "description" =>$description));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public function delete($id){
			$query = "DELETE FROM marital_status WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}