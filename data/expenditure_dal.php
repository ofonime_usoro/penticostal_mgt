<?php 
	require_once __DIR__."/expenditure.php";
	require_once __DIR__."/core.php";

	class ExpenditureDAL {
		private $expenditure;

		public function __construct($expenditure)
		{
			$this->expenditure = $expenditure;
		}

		public function insert(){
			$query = "INSERT INTO expenditure SET account_id = :account_id, amount = :amount, `date` = :date_entered, description = :description, user_id = :user_id, created = NOW()";
			$status = array();
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"account_id" => $this->expenditure->get_account_id(),
					"amount" => $this->expenditure->get_amount(),
					"date_entered" => $this->expenditure->get_date(),
					"description" => $this->expenditure->get_description(),
					"user_id" => $this->expenditure->get_user_id()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}

				//return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}


		public function fetch($id)
		{
			$query = "SELECT * FROM expenditure WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));

				$data = $result->fetch(PDO::FETCH_OBJ);
				
				$expenditure = new Expenditure();
				$expenditure->set_id($data->id);
				$expenditure->set_account_id($data->account_id);
				$expenditure->set_amount($data->amount);
				$expenditure->set_date($data->date);
				$expenditure->set_description($data->description);
				$expenditure->set_user_id($data->user_id);
				$expenditure->set_date_created($data->created);

				return $expenditure;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all()
		{
			$query = "SELECT * FROM expenditure";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update(){
			$query = "UPDATE expenditure SET account_id = :account_id, amount = :amount, `date` = :day, description = :description, user_id = :user_id, created = :created WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"account_id" => $this->expenditure->get_account_id(),
					"amount" => $this->expenditure->get_amount(),
					"day"  => $this->expenditure->get_date(	),
					"description" => $this->expenditure->get_description(),
					"user_id" => $this->expenditure->get_user_id(),
					"created" => $this->expenditure->get_date_created(),
					"id" => $this->expenditure->get_id()

				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete($id){
			$query = "DELETE FROM expenditure WHERE id = :id";
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete_by_account($account_id){
			$query = "DELETE FROM expenditure WHERE account_id = :account_id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("account_id" => intval($account_id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	}