<?php 
	require_once __DIR__."/lga.php";
	require_once __DIR__."/core.php";
	//require_once 


	class LGADAL {
		private $lga;

		public function __construct($lga){
			$this->lga = $lga;
		}
		//LGA CRUD Operation

		/** C CREATE ADD LGA**/

		public function insert() {
			$query = "INSERT INTO lgas SET name = :name";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array("name"=>$this->lga->get_name()));

				return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/**R READ or LIST **/

		public static function fetchById($id){
			//returns the LGA Model
			$query = "SELECT id, name FROM lgas WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array(
					"id" => intval($id)
				));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$lga = new LGA();
				$lga->set_id($data->id);
				$lga->set_name($data->name);

				return $lga;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all($state_id ){
			$query = "SELECT * FROM lgas WHERE state_id = :state_id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				
				$result->execute(array(
					"state_id" => $state_id
				));
				$data = $result->fetchAll(PDO::FETCH_OBJ);

				return $data;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update($lga_id, $new_value){
			$query = "UPDATE lgas SET name = :name WHERE id = :lga_id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array("name" => $new_value, "lga_id" =>$lga_id));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function delete($id){
			$query = "DELETE FROM lga WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array("id" =>$id));

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}