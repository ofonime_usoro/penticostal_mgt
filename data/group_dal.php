<?php 
	require_once __DIR__."/group.php";
	require_once __DIR__."/core.php";

	class GroupDAL {
		private $group;

		public function __construct($group)
		{
			$this->group = $group;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO `group` SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					":name" => $this->group->get_name(),
					":description" => $this->group->get_description()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name, description FROM `group` WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				

				$data = $result->fetch(PDO::FETCH_OBJ);
				$group = new Group();
				$group->set_id($data->id);
				$group->set_name($data->name);
				$group->set_description($data->description);

				return $group;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM `group`";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
			
		}

		/** -- U **/
		public function update(){
			$query = "UPDATE `group` SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->group->get_id()), 
					"name" => $this->group->get_name(), 
					"description" =>$this->group->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){

			$query = "DELETE FROM `group` WHERE id = :id";
			
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}