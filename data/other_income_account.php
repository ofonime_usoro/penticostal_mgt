<?php 

	class OtherIncomeAccount {
		private $id;
		private $category_type;
		private $name;
		private $description;
		private $created;
		private $updated;

		public function get_id(){return $this->id;}
		private function set_id($id){$this->id = $id;}
		public function get_category_type(){return $this->category_type;}
		private function set_category_type($category_type){$this->category_type = $category_type;}
		public function get_name(){return $this->name;}
		private function set_name($name){$this->name = $name;}
		public function get_desccription(){return $this->description;}
		private function set_description($description){$this->description = $description;}
		public function get_date_created(){return $this->created;}
		private function set_date_created($created){$this->created = $created;}
		public function get_date_updated(){return $this->updated;}
		private function set_date_updated($updated){$this->updated = $updated;}
	}