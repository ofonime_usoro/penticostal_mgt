<?php 
	require_once __DIR__."/other_income_type.php";
	require_once __DIR__."/core.php";

	class OtherIncomeTypeDAL {
		private $other_income_type;

		public function __construct($other_income_type)
		{
			$this->other_income_type = $other_income_type;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO other_income_type SET name = :name, description = :description, created = :created, updated = :updated";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					":name" => $this->other_income_type->get_name(),
					":description" => $this->other_income_type->get_description(),
					"created" => $this->other_income_type->get_date_created(),
					"updated" => $this->other_income_type->get_date_updated()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name, description, created, updated  FROM other_income_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$other_income_type = new OtherIncomeType();


				$other_income_type->set_id($data->id);
				$other_income_type->set_name($data->name);
				$other_income_type->set_description($data->description);
				$other_income_type->set_date_created($data->created);
				$other_income_type->set_date_updated($data->updated);

				return $other_income_type;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM other_income_type";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update(){
			
		}

		/** --D-- **/
		public function delete($id){
			require_once("other_income_dal.php");
			OtherIncomeDAL::delete_by_type($id);
			$query = "DELETE FROM other_income_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}