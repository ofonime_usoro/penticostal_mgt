<?php 

	class Member {
		private $id;
		private $tithe_card_no;
		private $first_name;
		private $middle_name;
		private $last_name;
		private $gender;
		private $date_of_birth;
		private $occupation;
		private $phone_no;
		private $email;
		private $address;
		private $state_id;
		private $lga_id;
		private $group_id;
		private $department_id;
		private $fellowship_id;
		private $assembly_id;
		private $date_baptized;
		private $date_born_again;
		private $spouse_name;
		private $spouse_phone_no;
		private $no_of_children;
		private $avatar_location;
	


		public function get_id()
		{
			return $this->id;
		}

		public function set_id($id){
			$this->id = $id;
		}

		public function get_tithe_card_no()
		{
			return $this->tithe_card_no;
		}

		public function set_tithe_card_no($tithe_card_no){
			$this->tithe_card_no = $tithe_card_no;
		}


		public function get_first_name()
		{
			return $this->first_name;
		}

		public function set_first_name($first_name)
		{
			$this->first_name = $first_name;
		}

		public function get_middle_name()
		{
			return $this->middle_name;
		}

		public function set_middle_name($middle_name)
		{
			$this->middle_name = $middle_name;
		}

		public function get_last_name()
		{
			return $this->last_name;
		}

		public function set_last_name($last_name)
		{
			$this->last_name = $last_name;
		}

		public function get_gender(){return $this->gender;}
		public function set_gender($gender){$this->gender = $gender;}

		public function get_date_of_birth(){return $this->date_of_birth;}
		public function set_date_of_birth($dob){$this->date_of_birth = $dob;}

		public function get_occupation(){return $this->occupation;}
		public function set_occupation($occupation){$this->occupation = $occupation;}

		public function get_phone_no(){return $this->phone_no;}
		public function set_phone_no($phone_no){$this->phone_no = $phone_no;}

		public function get_email(){ return $this->email;}
		public function set_email($email){$this->email = $email; }

		public function get_address(){return $this->address;}
		public function set_address($address){$this->address = $address; }

		public function get_state_id(){return $this->state_id;}
		public function set_state_id($state_id){$this->state_id = $state_id;}

		public function get_lga_id(){return $this->lga_id;}
		public function set_lga_id($lga_id){$this->lga_id = $lga_id;}

		public function get_group_id(){return $this->group_id;}
		public function set_group_id($group_id){$this->group_id = $group_id;}

		public function get_department_id(){return $this->department_id;}
		public function set_department_id($department_id){$this->department_id = $department_id;}

		public function get_fellowship_id(){return $this->fellowship_id;}
		public function set_fellowship_id($fellowship_id){$this->fellowship_id = $fellowship_id;}


		public function get_assembly_id(){return $this->assembly_id;}
		public function set_assembly_id($assembly_id){$this->assembly_id = $assembly_id;}

		public function get_date_baptized(){return $this->date_baptized;}
		public function set_date_baptized($date_baptized){$this->date_baptized = $date_baptized;}

		public function get_date_born_again(){return $this->date_born_again;}
		public function set_date_born_again($date_born_again){$this->date_born_again = $date_born_again;}

		public function get_spouse_name(){return $this->spouse_name;}
		public function set_spouse_name($spouse_name){$this->spouse_name = $spouse_name;}

		public function get_spouse_phone_no(){return $this->spouse_phone_no;}
		public function set_spouse_phone_no($spouse_phone_no){$this->spouse_phone_no = $spouse_phone_no;}

		public function get_no_of_children(){return $this->no_of_children;}
		public function set_no_of_children($no_of_children){ $this->no_of_children  = $no_of_children;}

		public function get_avatar_location() { return $this->avatar_location; }
		public function set_avatar_location($avatar_location) { $this->avatar_location = $avatar_location; }
		
	}