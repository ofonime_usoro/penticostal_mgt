<?php 

	class Tithe {
		private $id;
		private $member_id;
		private $card_no;
		private $amount;
		private $week_no;
		private $date;
		private $user_id;
		private $created;

		public function get_id()
		{
			return $this->id;
		}

		public function set_id($id)
		{
			$this->id = $id;
		}

		public function get_member_id()
		{
			return $this->member_id;
		}

		public function set_member_id($member_id)
		{
			$this->member_id = $member_id;
		}

		public function get_card_no()
		{
			return $this->card_no;
		}

		public function set_card_no($card_no)
		{
			$this->card_no = $card_no;
		}

		public function get_amount()
		{
			return $this->amount;
		}

		public function set_amount($amount){
			$this->amount = $amount;
		}
		
		public function get_week_no()
		{
			return $this->week_no;
		}

		public function set_week_no($week_no) 
		{
			$this->week_no = $week_no;
		}

		public function get_date()
		{
			return $this->date;
		}

		public function set_date($date)
		{
			$this->date = $date;
		}

		public function get_user_id()
		{
			return $this->user_id;
		}

		public function set_user_id($user_id)
		{
			$this->user_id = $user_id;
		}

		public function get_date_created()
		{
			return $this->created;
		}

		public function set_date_created($date)
		{
			$this->created = $date;
		}

	}