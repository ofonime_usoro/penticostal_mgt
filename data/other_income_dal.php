<?php 
	require_once __DIR__."/other_income.php";
	require_once __DIR__."/core.php";

	class OtherIncomeDAL {
		private $other_income;

		public function __construct($other_income)
		{
			$this->other_income = $other_income;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO other_income SET type_id = :type_id, amount = :amount, `date` = :date_entered, description = :description, user_id = :user_id, created = :created";

			try {	
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"type_id" => $this->other_income->get_type_id(),
					"amount" => $this->other_income->get_amount(),
					"date_entered" => $this->other_income->get_date(),
					"description" => $this->other_income->get_description(),
					"user_id" => $this->other_income->get_user_id(),
					"created" => $this->other_income->get_date_created()
				));
				
				if($stmt){
					return true;
				}else {
					return false;
				}

				var_dump($stmt);
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT * FROM other_income WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$other_income = new OtherIncome();

				$other_income->set_id($data->id);
				$other_income->set_type_id($data->type_id);
				$other_income->set_amount($data->amount);
				$other_income->set_date($data->date);
				$other_income->set_description($data->description);
				$other_income->set_user_id($data->user_id);
				$other_income->set_date_created($data->created);

				return $other_income;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM other_income";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update(){
			$query = "UPDATE `other_income` SET type_id= :type_id, amount = :amount, `date` = :day, description = :description, user_id = :user_id, created = :created WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->other_income->get_id()), 
					"type_id" => $this->other_income->get_type_id(), 
					"amount" => $this->other_income->get_amount(),
					"day" => $this->other_income->get_date(),
					"description" => $this->other_income->get_description(),
					"user_id" => $this->other_income->get_user_id(),
					"created" => $this->other_income->get_date_created()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}


		/** --D-- **/
		public static function delete($id){
			$query = "DELETE FROM other_income WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete_by_type($id){
			$query = "DELETE FROM other_income WHERE type_id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}