<?php
	require_once __DIR__."/member.php";
	require_once __DIR__."/core.php";


	class MemberDAL {
		private $member;

		public function __construct($member){
			$this->member = $member;
		}

		public function insert()
		{
			$query = "INSERT INTO member SET tithe_card_no = :tithe_card_no, first_name = :first_name, middle_name = :middle_name, last_name = :last_name, gender = :gender, date_of_birth = :date_of_birth, email = :email, phone_no = :phone_no, address = :address, state_id = :state_id, lga_id = :lga_id, group_id = :group_id, department_id = :department_id, fellowship_id = :fellowship_id, assembly_id = :assembly_id, date_baptized = :date_baptized, date_born_again = :date_born_again, spouse_name = :spouse_name, occupation = :occupation, spouse_phone_no = :spouse_phone_no, no_of_children = :no_of_children, avatar_location = :avatar_location";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"tithe_card_no" => $this->member->get_tithe_card_no(),
					"first_name" => $this->member->get_first_name(),
					"middle_name" => $this->member->get_middle_name(),
					"last_name" => $this->member->get_last_name(),
					"gender" => $this->member->get_gender(),
					"date_of_birth" => $this->member->get_date_of_birth(),
					"occupation" => $this->member->get_occupation(),
					"phone_no" => $this->member->get_phone_no(),
					"email" => $this->member->get_email(),
					"address" => $this->member->get_address(),
					"state_id" => $this->member->get_state_id(),
					"lga_id" => $this->member->get_lga_id(),
					"group_id" => $this->member->get_group_id(),
					"department_id" => $this->member->get_department_id(),
					"fellowship_id" => $this->member->get_fellowship_id(),
					"assembly_id" => $this->member->get_assembly_id(),
					"avatar_location" => $this->member->get_avatar_location(),
					"date_baptized" => $this->member->get_date_baptized(),
					"date_born_again" => $this->member->get_date_born_again(),
					"spouse_name" => $this->member->get_spouse_name(),
					"spouse_phone_no" => $this->member->get_spouse_phone_no(),
					"no_of_children" => $this->member->get_no_of_children()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){	
				echo $e->getMessage();
			}
		}

		public static function fetch($id){
			$query = "SELECT tithe_card_no, first_name, middle_name, last_name, gender, date_of_birth, occupation, email, phone_no, address, state_id, lga_id, group_id, department_id, fellowship_id, assembly_id, date_baptized, date_born_again, spouse_name, spouse_phone_no, no_of_children, avatar_location FROM member WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => $id));
				
				$data = $result->fetch(PDO::FETCH_OBJ);

				$avatar_location = "member/default.png";

				$member = new Member();
				$member->set_tithe_card_no($data->tithe_card_no);
				$member->set_first_name($data->first_name);
				$member->set_middle_name($data->middle_name);
				$member->set_last_name($data->last_name);
				$member->set_gender($data->gender);
				$member->set_date_of_birth($data->date_of_birth);
				$member->set_email($data->email);
				$member->set_phone_no($data->phone_no);
				$member->set_address($data->address);
				$member->set_state_id($data->state_id);
				$member->set_lga_id($data->lga_id);
				$member->set_group_id($data->group_id);
				$member->set_department_id($data->department_id);
				$member->set_fellowship_id($data->fellowship_id);
				$member->set_assembly_id($data->assembly_id);
				$member->set_department_id($data->date_baptized);
				$member->set_date_born_again($data->date_born_again);
				$member->set_spouse_name($data->spouse_name);
				$member->set_spouse_phone_no($data->spouse_phone_no);
				$member->set_no_of_children($data->no_of_children);
				$member->set_occupation($data->occupation);
				$member->set_avatar_location($data->avatar_location);
				
				return $member;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
			return null;
		}

		public function fetchAll(){
			$query = "SELECT * FROM member";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);
				
				$data = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetchByCardNumber($card_no){
			$query = "SELECT * FROM member WHERE tithe_card_no = :card_no";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				
				$result->execute(array(
					"card_no" => intval($card_no)
				));

				$data = $result->fetch(PDO::FETCH_OBJ);
				return $data;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete($id){
			require_once __DIR__."/tithe_dal.php";
			TitheDAL::delete_by_member($id);
			$member = self::fetch($id);

			if(file_exists('../uploads/'.$member->get_avatar_location())){
				unlink('../uploads/'.$member->get_avatar_location());
			}

			$query = "DELETE FROM member WHERE id=:id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update(){
			if($this->member->get_avatar_location() != ''){
				$member = $this->fetch($this->member->get_id());
				if($member->get_avatar_location() != '' && file_exists('../uploads/'.$member->get_avatar_location())){
					unlink('../uploads/'.$member->get_avatar_location());
				}
			}

			$query = "UPDATE `member` SET  tithe_card_no = :card_no, first_name = :first_name, middle_name = :middle_name, last_name = :last_name, gender = :gender, date_of_birth = :date_of_birth, occupation = :occupation, phone_no = :phone_no, email = :email, address = :address, state_id = :state_id, lga_id = :lga_id, group_id = :group_id, department_id = :department_id, fellowship_id = :fellowship_id, assembly_id = :assembly_id, avatar_location = :avatar_location, date_baptized = :date_baptized, date_born_again = :date_born_again, spouse_name = :spouse_name, spouse_phone_no = :spouse_phone_no, no_of_children = :no_of_children WHERE id = :member_id";

			try{
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"card_no" => $this->member->get_tithe_card_no(),
					"first_name" => $this->member->get_first_name(),
					"middle_name" => $this->member->get_middle_name(),
					"last_name" => $this->member->get_last_name(),
					"gender" => $this->member->get_gender(),
					"date_of_birth" => $this->member->get_date_of_birth(),
					"occupation" => $this->member->get_occupation(),
					"phone_no" => $this->member->get_phone_no(),
					"email" => $this->member->get_email(),
					"address" => $this->member->get_address(),
					"state_id" => $this->member->get_state_id(),
					"lga_id" => $this->member->get_lga_id(),
					"group_id" => $this->member->get_group_id(),
					"department_id" => $this->member->get_department_id(),
					"fellowship_id" => $this->member->get_fellowship_id(),
					"assembly_id" => $this->member->get_assembly_id(),
					"avatar_location" => $this->member->get_avatar_location(),
					"date_baptized" => $this->member->get_date_baptized(),
					"date_born_again" => $this->member->get_date_born_again(),
					"spouse_name" => $this->member->get_spouse_name(),
					"spouse_phone_no" => $this->member->get_spouse_phone_no(),
					"no_of_children" => $this->member->get_no_of_children(),
					"member_id" => $this->member->get_id()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		
	}