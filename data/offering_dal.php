<?php 
	require_once __DIR__."/offering.php";
	require_once __DIR__."/core.php";

	class OfferingDAL {
		private $offering;

		public function __construct($offering){
			$this->offering = $offering;
		}

		public function insert(){
		    $query = "INSERT INTO offering SET type_id = :type_id, amount = :amount, `date` = :date_entered, description = :description, created = NOW(), user_id = :user_id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"type_id" => $this->offering->get_type_id(),
					"amount" => $this->offering->get_amount(),
					"date_entered" => $this->offering->get_date(),
					"description" => $this->offering->get_description(),
					"user_id" => $this->offering->get_user_id()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}

				return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT * FROM offering WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));

				$data = $result->fetch(PDO::FETCH_OBJ);
					
				$offering = new Offering();
				$offering->set_type_id($data->type_id);
				$offering->set_amount($data->amount);
				$offering->set_date($data->date);
				$offering->set_description($data->description);
				$offering->set_user_id($data->user_id);
				$offering->set_date_created($data->created);

				return $offering;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM offering";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update(){
			$query = "UPDATE offering SET type_id = :type_id, user_id = :user_id, amount = :amount, `date` = :day, description = :description, created = :created WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"type_id" => intval($this->offering->get_type_id()), 
					"user_id" => $this->offering->get_user_id(), 
					"amount" => $this->offering->get_amount(),
					"day" => $this->offering->get_date(),
					"description" =>$this->offering->get_description(),
					"created" => $this->offering->get_date_created(),
					"id" => $this->offering->get_id()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){
			$query = "DELETE FROM offering WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	}