<?php 
	require_once __DIR__."/expenditure_account.php";
	require_once __DIR__."/core.php";

	class ExpenditureAccountDAL {
		private $expenditure_account;

		public function __construct($expenditure_account){
			$this->expenditure_account = $expenditure_account;
		}

		public function insert(){
			$query = "INSERT INTO expenditure_account SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					":name" => $this->expenditure_account->get_name(),
					":description" => $this->expenditure_account->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name, description FROM expenditure_account WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$expenditure_account = new ExpenditureAccount();
				$expenditure_account->set_id($data->id);
				$expenditure_account->set_name($data->name);
				$expenditure_account->set_description($data->description);

				return $expenditure_account;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM expenditure_account";
		
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_id_by_name($name){
			$query = "SELECT id FROM expenditure_account WHERE name = :name";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $name
				));
				$data = $result->fetch(PDO::FETCH_OBJ);
				echo $data->id;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update(){
			$query = "UPDATE expenditure_account SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->expenditure_account->get_id()), 
					"name" => $this->expenditure_account->get_name(), 
					"description" =>$this->expenditure_account->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){
			require_once("expenditure_dal.php");
			ExpenditureDAL::delete_by_account($id);
			$query = "DELETE FROM expenditure_account WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	}