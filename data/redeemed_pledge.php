<?php 

	class RedeemedPledge {
		private $id;
		private $pledge_id;
		private $amount;
		private $date;
		private $user_id;
		private $created;

		public function get_id(){return $this->id;}
		public function set_id($id){$this->id = $id;}
		public function get_pledge_id(){return $this->pledge_id;}
		public function set_pledge_id($pledge_id){$this->pledge_id = $pledge_id;}
		public function get_amount(){return $this->amount;}
		public function set_amount($amount){$this->amount = $amount;}
		public function get_date(){return $this->date;}
		public function set_date($date){$this->date = $date;}
		public function get_user_id(){return $this->user_id;}
		public function set_user_id($user_id){$this->user_id = $user_id;}
		public function get_date_created(){return $this->created;}
		public function set_date_created($created){$this->created = $created;}

	}
?>
