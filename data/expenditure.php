<?php 

	class Expenditure {
		private $id;
		private $account_id;
		private $amount;
		private $date;
		private $description;
		private $user_id;
		private $created;

		public function get_id()
		{
			return $this->id;
		}

		public function set_id($id)
		{
			$this->id = $id;
		}

		public function get_account_id()
		{
			return $this->account_id;
		}

		public function set_account_id($account_id)
		{
			$this->account_id = $account_id;
		}

		public function get_amount()
		{
			return $this->amount;
		}

		public function set_amount($amount)
		{
			$this->amount = $amount;
		}

		public function get_description()
		{
			return $this->description;
		}

		public function set_description($description)
		{
			$this->description = $description;
		}

		public function get_user_id()
		{
			return $this->user_id;
		}
		
		public function set_user_id($user_id)
		{
			$this->user_id = $user_id;
		}

		public function get_date()
		{
			return $this->date;
		}

		public function set_date($date)
		{
			$this->date = $date;
		}

		public function get_date_created()
		{
			return $this->created;
		}

		public function set_date_created($date_created)
		{
			$this->created = $date_created;
		}

	}