<?php 
	require_once __DIR__."/fellowship.php";
	require_once __DIR__."/core.php";
	
	class FellowshipDAL {
		private $fellowship;

		public function __construct($fellowship){
			$this->fellowship = $fellowship;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO fellowship SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->fellowship->get_name(),
					"description" => $this->fellowship->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name, description FROM fellowship WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
			
				$data = $result->fetch(PDO::FETCH_OBJ);
				$fellowship = new Fellowship();
				$fellowship->set_id($data->id);
				$fellowship->set_name($data->name);
				$fellowship->set_description($data->description);

				return $fellowship;
				
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all()
		{
			$query = "SELECT * FROM fellowship";
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public static function update($fellowship, $fellowship_id){
			$query = "UPDATE fellowship SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($fellowship_id), 
					"name" => $fellowship->get_name(), 
					"description" =>$fellowship->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){
			$query = "DELETE FROM fellowship WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}