<?php 
	require_once __DIR__."/department.php";
	require_once __DIR__."/core.php";

	class DepartmentDAL {
		private $department;

		public function __construct($department){
			$this->department = $department;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO `department` SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->department->get_name(),
					"description" => $this->department->get_description()
				));
				if($stmt) {
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name, description FROM department WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$department = new Department();
				$department->set_id($data->id);
				$department->set_name($data->name);
				$department->set_description($data->description);

				return $department;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM department";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update(){
			$query = "UPDATE department SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->department->get_id()), 
					"name" => $this->department->get_name(), 
					"description" =>$this->department->get_description()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){
			$query = "DELETE FROM department WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}