<?php 
	require_once __DIR__."/fund_raising.php";
	require_once __DIR__."/core.php";

	
	class FundRaisingDAL {
		private $fund_raising;

		public function __construct($fund_raising){
			$this->fund_raising = $fund_raising;
		}

		public function insert(){
			$query = "INSERT INTO fund_raising SET fund_raising_id = :f_id, amount = :amount, amount_redeemed = :amount_redeemed, `date` = :date_entered, name = :name, phone_no = :phone_no, address = :address, created = :created, user_id = :user_id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"f_id" => $this->fund_raising->get_fund_raising_id(),
					"amount" => $this->fund_raising->get_amount(),
					"amount_redeemed" => $this->fund_raising->get_amount_redeemed(),
					"date_entered" => $this->fund_raising->get_date(),
					"name" => $this->fund_raising->get_name(),
					"phone_no" => $this->fund_raising->get_phone_no(),
					"address" => $this->fund_raising->get_address(),
					"created" => $this->fund_raising->get_date_created(),
					"user_id" => $this->fund_raising->get_user_id()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch($id)
		{
			$query = "SELECT * FROM fund_raising WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$fund_raising = new FundRaising();
				$fund_raising->set_id($data->id);
				$fund_raising->set_fund_raising_id($data->fund_raising_id);
				$fund_raising->set_amount($data->amount);
				$fund_raising->set_amount_redeemed($data->amount_redeemed);
				$fund_raising->set_date($data->date);
				$fund_raising->set_name($data->name);
				$fund_raising->set_phone_no($data->phone_no);
				$fund_raising->set_address($data->address);
				$fund_raising->set_date_created($data->created);
				$fund_raising->set_user_id($data->user_id);

				return $fund_raising;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM fund_raising";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update(){
			$query = "UPDATE fund_raising SET fund_raising_id = :fund_raising_id,amount = :amount, amount_redeemed = :amount_redeemed, `date` = :day, name = :name, phone_no = :phone_no, address = :address, created = :created, user_id = :user_id WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->fund_raising->get_id()), 
					"fund_raising_id" => $this->fund_raising->get_fund_raising_id(), 
					"amount" =>$this->fund_raising->get_amount(),
					"amount_redeemed" => $this->fund_raising->get_amount_redeemed(),
					"day" => $this->fund_raising->get_date(),
					"name" => $this->fund_raising->get_name(),
					"phone_no" => $this->fund_raising->get_phone_no(),
					"address" => $this->fund_raising->get_address(),
					"created" => $this->fund_raising->get_date_created(),
					"user_id" => $this->fund_raising->get_user_id()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public function delete($id){
			$query = "DELETE FROM fund_raising WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete_by_fund_id($fund_id){
			$query = "DELETE FROM fund_raising WHERE fund_raising_id = :fund_id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("fund_id" => intval($fund_id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	}