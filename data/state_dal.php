<?php 
	require_once __DIR__."/state.php";
	require_once __DIR__."/core.php";

	class StateDAL {
		private $state;

		public function __construct($state){
			$this->state = $state;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO state SET name = :name";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->state->get_name()
				));
				return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT id, name FROM `state` WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$state = new State();
				$state->set_id($data->id);
				$state->set_name($data->name);

				return $state;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM state";
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update($id, $name, $description){
			$query = "UPDATE state SET name = :name WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array("id" => intval($id), "name" => $name));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public function delete($id){
			$query = "DELETE FROM state WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}