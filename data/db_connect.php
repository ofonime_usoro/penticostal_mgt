<?php 

	class DB_CONNECT {
		public $db;

		function __construct(){
			//connecting to the database
			require_once __DIR__."/db_config.php";
			try {
				$this->db = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_DATABASE, DB_USER, DB_PASSWORD);	
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				echo "Hello World";
			}catch(PDOException $e){
				echo $e->getMessage();
			}
			
		}
	}