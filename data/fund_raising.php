<?php 
	
	class FundRaising {
		private $id;
		private $fund_raising_id;
		private $user_id;
		private $amount;
		private $amount_redeemed;
		private $date;
		private $name;
		private $phone_no;
		private $address;
		private $created;

		public function get_id(){return $this->id;}
		public function set_id($id){$this->id = $id;}
		public function get_fund_raising_id(){return $this->fund_raising_id;}
		public function set_fund_raising_id($fd_id){$this->fund_raising_id = $fd_id;}
		public function get_user_id(){return $this->user_id;}
		public function set_user_id($user_id){$this->user_id = $user_id;}
		public function get_amount(){return $this->amount;}
		public function set_amount($amount){$this->amount = $amount;}
		public function get_amount_redeemed(){return $this->amount_redeemed;}
		public function set_amount_redeemed($amount_redeemed){$this->amount_redeemed = $amount_redeemed;}

		public function get_date(){return $this->date;}
		public function set_date($date){$this->date = $date;}

		public function get_name(){return $this->name;}
		public function set_name($name){$this->name = $name; }

		public function get_phone_no(){return $this->phone_no;}
		public function set_phone_no($phone_no){$this->phone_no = $phone_no; }
		public function get_address(){return $this->address;}
		public function set_address($address){$this->address = $address;}
		public function get_date_created(){return $this->created;}
		public function set_date_created($created){$this->created = $created; }
	}
?>