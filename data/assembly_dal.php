<?php 
	require_once __DIR__."/assembler.php";
	require_once __DIR__."/core.php";

	class AssemblyDAL {
		private $assembly;

		public function __construct($assembly){
			$this->assembly = $assembly;
		}

		public function insert(){
			$query = "INSERT INTO assembly SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->assembly->get_name(),
					"description" => $this->assembly->get_description()
				));
				
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public static function fetch($id){
			$query = "SELECT id, name, description FROM assembly WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));

				$data = $result->fetch(PDO::FETCH_OBJ);
				$assembly = new Assembly();
				$assembly->set_id($data->id);
				$assembly->set_name($data->name);
				$assembly->set_description($data->description);

				return $assembly;
				
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM assembly";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function update(){

			$query = "UPDATE assembly SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->assembly->get_id()),
					"name" => $this->assembly->get_name(),
					"description" =>$this->assembly->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public static function delete($id){
			$query = "DELETE FROM assembly WHERE id = :id";
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}