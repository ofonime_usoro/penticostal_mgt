<?php 
	require_once __DIR__."/tithe.php";
	require_once __DIR__."/core.php";

	class TitheDAL {
		private $tithe;

		public function __construct($tithe){
			$this->tithe = $tithe;
		}

		public function insert(){
			$query = "INSERT INTO tithe SET member_id = :member_id, card_no = :card_no, amount = :amount, week_no = :week_no, `date` = :date_entered, user_id = :user_id, created = :date_created";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"member_id" => $this->tithe->get_member_id(),
					"card_no" => $this->tithe->get_card_no(),
					"amount" => $this->tithe->get_amount(),
					"week_no" => $this->tithe->get_week_no(),
					"date_entered" => $this->tithe->get_date(),
					"user_id" => $this->tithe->get_user_id(),
					"date_created" => $this->tithe->get_date_created()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT * FROM `tithe` WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				
				$data = $result->fetch(PDO::FETCH_OBJ);

				$tithe = new Tithe();
				$tithe->set_id($data->id);
				$tithe->set_member_id($data->member_id);
				$tithe->set_card_no($data->card_no);
				$tithe->set_amount($data->amount);
				$tithe->set_week_no($data->week_no);
				$tithe->set_date($data->date);
				$tithe->set_user_id($data->user_id);
				$tithe->set_date_created($data->created);


				return $tithe;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all()
		{
			$query = "SELECT * FROM tithe";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public static function update($tithe, $tithe_id){
			$query = "UPDATE `tithe` SET member_id= :member_id, user_id = :user_id, card_no = :card_no, amount = :amount, week_no = :week_no, `date` = :day, created = :created WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($tithe_id), 
					"member_id" => $tithe->get_member_id(), 
					"user_id" =>$tithe->get_user_id(),
					"card_no" => $tithe->get_card_no(),
					"amount" => $tithe->get_amount(),
					"week_no" => $tithe->get_week_no(),
					"day" => $tithe->get_date(),
					"created" => $tithe->get_date_created()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public static function delete($id){
			//MemberDAL::delete_by_tithe_id($id);
			//$tithe = self::fetch($id);
			$query = "DELETE FROM `tithe` WHERE id=:id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function delete_by_member($member_id){
			$query = "DELETE FROM tithe WHERE member_id = :member_id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("member_id" => intval($member_id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	}