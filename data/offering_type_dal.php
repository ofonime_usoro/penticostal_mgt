<?php
	require_once __DIR__."/offering_type.php";
	require_once __DIR__."/core.php";

	
	class OfferingTypeDAL {
		private $offering_type;

		public function __construct($offering_type){
			$this->offering_type = $offering_type;
		}

		public function insert(){
			$query = "INSERT INTO offering_type SET name = :name, description = :description";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					":name" => $this->offering_type->get_name(),
					":description" => $this->offering_type->get_description()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public function fetch($id)
		{
			$query = "SELECT id, name, description FROM offering_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$offering_type = new OfferingType();
				$id = $offering_type->set_id($data->id);
				$name = $offering_type->set_name($data->name);
				$description = $offering_type->set_description($data->description);

				return $offering_type;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM offering_type";
			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_id_by_name($name){
			$query = "SELECT id FROM offering_type WHERE name = :name";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $name
				));
				$data = $result->fetch(PDO::FETCH_OBJ);
				echo $data->id;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/
		public function update(){
			$query = "UPDATE offering_type SET name = :name, description = :description WHERE id = :id";
			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"id" => intval($this->offering_type->get_id()), 
					"name" => $this->offering_type->get_name(), 
					"description" =>$this->offering_type->get_description()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public function delete($id){
			$query = "DELETE FROM offering_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}