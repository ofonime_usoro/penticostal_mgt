<?php 

	class Config {
		static $confArray;

		public static function read($name){
			return self::$confArray[$name];
		}

		public static function write($name, $value){
			self::$confArray[$name] = $value;
		}
	}

	//db
	Config::write('db.host', '127.0.0.1');
	Config::write('db.port', '80');
	Config::write('db.basename', 'penticostal');
	Config::write('db.user', 'root');
	Config::write('db.password', '');