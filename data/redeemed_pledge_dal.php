<?php 
	require_once __DIR__."/redeemed_pledge.php";
	require_once __DIR__."/core.php";

	class RedeemedPledgeDAL {
		private $redeemed_pledge;

		public function __construct($redeemed_pledge)
		{
			$this->redeemed_pledge = $redeemed_pledge;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO redeemed_pledge SET pledge_id = :pledge_id, amount = :amount, `date` = :date_entered, user_id = :user_id, created = :created";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					":pledge_id" => $this->redeemed_pledge->get_pledge_id(),
					":amount" => $this->redeemed_pledge->get_amount(),
					":date_entered" => $this->redeemed_pledge->get_date(),
					"user_id" => $this->redeemed_pledge->get_user_id(),
					"created" => $this->redeemed_pledge->get_date_created()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
				
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public static function fetch($id)
		{
			$query = "SELECT * FROM redeemed_pledge WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array("id" => intval($id)));
				$data = $result->fetch(PDO::FETCH_OBJ);
				
				$redeemed_pledge = new RedeemedPledge();
				$redeemed_pledge->set_pledge_id($data->pledge_id);
				$redeemed_pledge->set_amount($data->amount);
				$redeemed_pledge->set_date($data->date);
				$redeemed_pledge->set_user_id($data->user_id);
				$redeemed_pledge->set_date_created($data->created);
				
				return $redeemed_pledge;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public static function fetch_all()
		{
			$query = "SELECT * FROM redeemed_pledge";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** -- U **/

		public function update(){
			$query = "UPDATE redeemed_pledge SET pledge_id = :pledge_id, amount = :amount, `date` = :day, user_id = :user_id, created = :created WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"pledge_id" => $this->redeemed_pledge->get_pledge_id(),
					"amount" => $this->redeemed_pledge->get_amount(),
					"day" => $this->redeemed_pledge->get_date(),
					"user_id" => $this->redeemed_pledge->get_user_id(),
					"created" => $this->redeemed_pledge->get_date_created(),
					"id" => $this->redeemed_pledge->get_id()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		

		/** --D-- **/
		public static function delete($id){
			$query = "DELETE FROM redeemed_pledge WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}