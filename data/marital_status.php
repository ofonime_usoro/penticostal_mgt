<?php 

	class MaritalStatus {
		private $id;
		private $name;
		private $description = null;



		public function get_id()
		{
			return $this->id;
		}

		private function set_id($id){
			$this->id = $id;
		}


		public function get_name()
		{
			return $this->name;
		}

		private function set_name($name)
		{
			$this->name = $name;
		}

		public function get_description()
		{
			return $this->description;
		}

		private function set_description($description)
		{
			$this->description = $description;
		}
	}