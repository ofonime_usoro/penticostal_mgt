<?php
	require_once __DIR__."/user.php";
	require_once __DIR__."/core.php";
	require __DIR__."/password_hash.php";

	class UserDAL {
		private $user;

		public function __construct($user){
			$this->user = $user;
		}

		public function insert(){
			$query = "INSERT INTO `user` SET username = :username, email = :email, `password` = :password, full_name = :full_name, address = :address, phone_no = :phone_no, is_admin = :is_admin, created = :created, updated = :updated";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"username" => $this->user->get_username(),
					"email" => $this->user->get_email(),
					"password" => $this->user->get_password(),
					"full_name" => $this->user->get_full_name(),
					"address" => $this->user->get_address(),
					"phone_no" => $this->user->get_phone_no(),
					"is_admin" => $this->user->get_is_admin(),
					"created" => $this->user->get_date_created(),
					"updated" => $this->user->get_date_updated()
 				));
 				if($stmt){
 					return true;
 				}else {
 					return false;
 				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch($id)
		{
			$query = "SELECT * FROM user WHERE id = :id";

			try 
			{
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array(
					"id" => intval($id)
				));

				$data = $result->fetch(PDO::FETCH_OBJ);

				$user = new User();
				$user->set_id($data->id);
				$user->set_username($data->username);
				$user->set_email($data->email);
				$user->set_full_name($data->full_name);
				$user->set_password($data->password);
				$user->set_address($data->address);
				$user->set_phone_no($data->phone_no);
				$user->set_is_admin($data->is_admin);
				$user->set_date_created($data->created);
				$user->set_date_updated($data->updated);

				return $user;
			}catch(PDOException $e)
			{
				echo $e->getMessage();
			}

		}

		public function fetchAll()
		{
			$query = "SELECT * FROM user";

			try 
			{
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);
				$data = $stmt->fetch(PDO::FETCH_OBJ);

				return $data;

			}catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}


		public function update($id, $username, $password, $full_name, $address, $phone_no, $is_admin, $created)
		{
			$query = "UPDATE user SET username = :username, password = :password, full_name = :full_name, address = :address, phone_no = :phone_no, is_admin = :is_admin, created = :created, updated = NOW() WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare();
				$stmt = $result->execute(array(
					"username" => $username,
					"password" => PasswordHash::getHash($password),
					"full_name" => $full_name,
					"address" => $address,
					"phone_no" => $phone_no,
					"is_admin" => $is_admin,
					"created" => $created
				));
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function delete($id){
			$query = "DELETE FROM user WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}