<?php 

	class Department {
		private $id;
		private $name;
		private $description;

		public function __construct(){}

		public function get_id()
		{
			return $this->id;
		}

		public function set_id($id){
			$this->id = $id;
		}


		public function get_name()
		{
			return $this->name;
		}

		public function set_name($name)
		{
			$this->name = $name;
		}

		public function get_description()
		{
			return $this->description;
		}

		public function set_description($description)
		{
			$this->description = $description;
		}
	}