<?php 

	class OtherIncomeType {
		private $id;
		private $name;
		private $description;
		private $created;
		private $updated;

		public function get_id(){return $this->id;}
		public function set_id($id){$this->id = $id;}
		public function get_name(){return $this->name;}
		public function set_name($name){$this->name = $name;}
		public function get_description(){return $this->description;}
		public function set_description($description){$this->description = $description;}
		public function get_date_created(){return $this->created;}
		public function set_date_created($date_created){$this->created = $date_created;}
		public function get_date_updated(){return $this->updated;}
		public function set_date_updated($updated){$this->updated = $updated;}
	}
?>

