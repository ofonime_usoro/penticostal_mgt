<?php 
	require_once __DIR__."/other_income_account.php";
	require_once __DIR__."/core.php";

	class OtherIncomeAccountDAL {
		private $other_income_account;

		public function __construct($other_income_account)
		{
			$this->other_income_account = $other_income_account;
		}

		//CRUD

		public function insert(){
			$query = "INSERT INTO other_income_account SET category_type = :category_type, name = :name, description = :description, created = :created, updated = :updated";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"category_type" => $this->other_income_account->get_category_type(),
					"name" => $this->other_income_account->get_name(),
					"description" => $this->other_income_account->get_description(),
					"created" => $this->other_income_account->get_date_created(),
					"updated" => $this->other_income_account->get_date_updated()
				));
				return $core->dbh->lastInsertId();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** Read -- fetch and fetchAll **/
		public function fetch($id)
		{
			$query = "SELECT * FROM other_income_account WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array("id" => intval($id)));
				if($stmt){
					//get the data if the params are bound successfully

					$data = $stmt->fetch(PDO::FETCH_OBJ);
					$other_income_account = new OtherIncomeAccount();
					$other_income_account->set_category_type($data->category_type);
					$other_income_account->set_name($data->name);
					$other_income_account->set_description($data->description);
					$other_income_account->set_date_created($data->created);
					$other_income_account->set_date_updated($data->updated);

					return $other_income_account;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all()
		{
			$query = "SELECT * FROM other_income_account";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->query($query);

				$data_fetched = $stmt->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		/** --D-- **/
		public function delete($id){
			$query = "DELETE FROM other_income_account WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);
				$stmt->execute(array("id" => intval($id)));
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}