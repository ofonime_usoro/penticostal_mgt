<?php 

	class FundRaisingType {
		private $id;
		private $user_id;
		private $name;
		private $description;
		private $date;
		private $amount_realized; 
		private $created;

		public function get_id(){return $this->id;}
		public function set_id($id){$this->id = $id;}
		public function get_user_id(){return $this->user_id;}
		public function set_user_id($user_id){$this->user_id = $user_id;}
		public function get_name(){return $this->name;}
		public function set_name($name){$this->name = $name;}
		public function get_description(){return $this->description;}
		public function set_description($description){$this->description = $description;}
		public function get_date(){return $this->date;}
		public function set_date($date){$this->date = $date;}

		public function get_amount_realized(){return $this->amount_realized;}
		public function set_amount_realized($amount_realized){$this->amount_realized = $amount_realized;}
		public function get_date_created(){return $this->created;}
		public function set_date_created($created){$this->created = $created;}
	}