<?php 
	require_once __DIR__."/fund_raising_type.php";
	require_once __DIR__."/core.php";

	class FundraisingTypeDAL {
		private $fund_raising_type;

		public function __construct($fund_raising_type){
			$this->fund_raising_type = $fund_raising_type;
		}

		public function insert(){
			$query = "INSERT INTO fund_raising_type SET name = :name, description = :description, `date` = :date_entered, amount_realized = :amount_realized, user_id = :user_id, created = :created";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$stmt = $result->execute(array(
					"name" => $this->fund_raising_type->get_name(),
					"description" => $this->fund_raising_type->get_description(),
					"date_entered" => $this->fund_raising_type->get_date(),
					"amount_realized" => $this->fund_raising_type->get_amount_realized(),
					"user_id" => $this->fund_raising_type->get_user_id(),
					"created" => $this->fund_raising_type->get_date_created()
				));
				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch($id){
			$query = "SELECT * FROM fund_raising_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);
				$result->execute(array(
					"id" => intval($id)
				));
				$data = $result->fetch(PDO::FETCH_OBJ);

				$fund_raising_type = new FundRaisingType();
				$fund_raising_type->set_name($data->name);
				$fund_raising_type->set_description($data->description);
				$fund_raising_type->set_date($data->date);
				$fund_raising_type->set_amount_realized($data->amount_realized);
				$fund_raising_type->set_user_id($data->user_id);
				$fund_raising_type->set_date_created($data->created);

				return $fund_raising_type;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function fetch_all(){
			$query = "SELECT * FROM fund_raising_type";

			try {
				$core = Core::getInstance();
				$result = $core->dbh->query($query);

				$data_fetched = $result->fetchAll(PDO::FETCH_OBJ);
				return $data_fetched;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function update()
		{
			$query = "UPDATE fund_raising_type SET name = :name, description = :description, `date` = :day, amount_realized = :amount_realized, user_id = :user_id, created = :created WHERE id = :id";

			try{
				$core = Core::getInstance();
				$result = $core->dbh->prepare($query);

				$stmt = $result->execute(array(
					"name" => $this->fund_raising_type->get_name(),
					"description" => $this->fund_raising_type->get_description(),
					"day" => $this->fund_raising_type->get_date(),
					"amount_realized" => $this->fund_raising_type->get_amount_realized(),
					"user_id" => $this->fund_raising_type->get_user_id(),
					"created" => $this->fund_raising_type->get_date_created(),
					"id" => $this->fund_raising_type->get_id()
				));

				if($stmt){
					return true;
				}else {
					return false;
				}

			}catch(PDOException $e){
				echo $e->getMessage();
			}
			
		}

		public static function delete($id){
			require_once("fund_raising_dal.php");
			FundRaisingDAL::delete_by_fund_id($id);
			$query = "DELETE FROM fund_raising_type WHERE id = :id";

			try {
				$core = Core::getInstance();
				$stmt = $core->dbh->prepare($query);

				$stmt->execute(array(
					"id" => intval($id)
				));

				if($stmt){
					return true;
				}else {
					return false;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}