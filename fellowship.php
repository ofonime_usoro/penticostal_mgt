<?php
    session_start();
    require_once("data/fellowship.php");
    require_once("data/fellowship_dal.php");


    $status = array();
    $fellowship = null;

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    if(isset($_GET['fellowship_id'])){
        $fellowship_id = strip_tags($_GET['fellowship_id']);
        $fellowship = FellowshipDAL::fetch($fellowship_id);
    }

    if(isset($_POST['update'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName'])){
            $name = strip_tags($_POST['txtName']);
            $description = strip_tags($_POST['txtDescription']);

            $updated_fellowship = new Fellowship();
            $updated_fellowship->set_name($name);
            $updated_fellowship->set_description($description);

            $flag = FellowshipDAL::update($updated_fellowship, $fellowship_id);
            if($flag == 1)
            {
                $status['style'] = 'alert-success';
                $status['title'] = 'Success';
                $status['message'] = 'Your Fellowship was updated successfully!';
            }
            else if($flag == 0)
            {
                $status['style'] = 'alert-error';
                $status['title'] = 'Error';
                $status['message'] = 'Error Inserting Data. Try again';
            }
        }
    }

    if(isset($_POST['submit'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName'])){
            $fellowship_name = strip_tags($_POST['txtName']);
            $fellowship_description = strip_tags($_POST['txtDescription']);
            
            $fellowship = new Fellowship();
            $fellowship->set_name($fellowship_name);
            $fellowship->set_description($fellowship_description);
            $fellowship_model = new FellowshipDAL($fellowship);
            $flag = $fellowship_model->insert();

            if($flag == 1)
            {
                $status['style'] = 'alert-success';
                $status['title'] = 'Success';
                $status['message'] = 'Your message was saved successfully!';
            }
            else if($flag == 0)
            {
                $status['style'] = 'alert-error';
                $status['title'] = 'Error';
                $status['message'] = 'Error Inserting Data. Try again';
            }

        }
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(1, 8);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Fellowship</h1>
                        <h4>Form Fellowship</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Fellowship</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Fellowship Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal"  method="post">
                                    <div class="control-group">
                                        <label for="txtName" class="control-label">Name</label>
                                        <div class="controls">
                                            <input type="text" required class="input-xlarge" name="txtName" id="txtName" data-rule-maxlength="50" data-rule-required="true"
                                            value="<?php echo ($fellowship != null) ? $fellowship->get_name() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" id="txtDescription" placeholder="Enter Description"></textarea>
                                        </div>
                                    </div>     
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['fellowship_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' name="<?php if(isset($_GET['fellowship_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                
                <footer>
                    <p>2016 © copyright. All rights reserved.</p>
                </footer>

                <a id="btn-scrollup" class="btn btn-circle btn-large" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->


        <!--basic scripts-->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="assets/jquery/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="assets/bootstrap/bootstrap.min.js"></script>
        <script src="assets/nicescroll/jquery.nicescroll.min.js"></script>

        <!--page specific plugin scripts-->
        <script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="assets/jquery-validation/dist/additional-methods.min.js"></script>

        <!--flaty scripts-->
        <script src="js/flaty.js"></script>

    </body>
</html>
