<?php 
	session_start();

	unset($_SESSION['user_id']);
	unset($_SESSION['username']);
	unset($_SESSION['full_name']);
	unset($_SESSION);

	session_destroy();

	header("Location: login.php");
	exit();