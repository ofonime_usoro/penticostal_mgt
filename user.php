<?php 
    session_start();
    $status = array();
    
    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    if(isset($_POST['submit'])){
        if(isset($_POST['txtFullName']) && !empty($_POST['txtFullName']) && isset($_POST['txtUsername']) && !empty($_POST['txtUsername'])  && isset($_POST['txtAddress']) && !empty($_POST['txtAddress']) && isset($_POST['txtEmail']) && !empty($_POST['txtEmail'])  && isset($_POST['txtPassword']) && !empty($_POST['txtPassword']) && isset($_POST['txtPhoneNo']) && !empty($_POST['txtPhoneNo'])){
            $full_name = strip_tags($_POST['txtFullName']);
            $username = strip_tags($_POST['txtUsername']);
            $email = strip_tags($_POST['txtEmail']);
            $address = strip_tags($_POST['txtAddress']);
            $password = strip_tags($_POST['txtPassword']);
            $phone_no = strip_tags($_POST['txtPhoneNo']);


            require_once("data/user.php");
            require_once("data/user_dal.php");
            require_once("data/password_hash.php");

            $user = new User();

            $user->set_email($email);
            $user->set_full_name($full_name);
            $user->set_username($username);
            $user->set_address($address);
            $user->set_phone_no($phone_no);
            $user->set_is_admin("0");
            $user->set_password(PasswordHash::getHash($password));
            $user->set_date_created(date('Y-m-d H:i:s'));
            $user->set_date_updated(date('Y-m-d H:i:s'));

            $user_model = new UserDAL($user);
            $flag =$user_model->insert();

            if($flag == 1)
            {
                $status['style'] = 'alert-success';
                $status['title'] = 'Success';
                $status['message'] = 'Your message was saved successfully!';
            }
            else if($flag == 0)
            {
                $status['style'] = 'alert-error';
                $status['title'] = 'Error';
                $status['message'] = 'Error Inserting Data. Try again';
            }
        }
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(6, 0);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Validation</h1>
                        <h4>Form validation sample</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Validation</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Validation Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="post">

                                    <div class="control-group">
                                        <label class="control-label" for="txtFullName">Full Name:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtFullName" id="txtFullName" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtUsername">Username:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtUsername" id="txtUsername" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtAddress">Address:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtAddress" id="txtAddress" class="input-xlarge" data-rule-required="true" data-rule-email="true" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtPhoneNo">Phone Number:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtPhoneNo" id="txtPhoneNo" class="input-xlarge" data-rule-required="true" data-rule-email="true" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtEmail">Email Address:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="email" name="txtEmail" id="txtEmail" class="input-xlarge" data-rule-required="true" data-rule-email="true" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtPassword">Password:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="password" name="txtPassword" id="txtPassword" class="input-xlarge" data-rule-required="true" data-rule-minlength="6" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>