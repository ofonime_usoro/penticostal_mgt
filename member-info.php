<?php 
    session_start();
    require_once("data/member.php");
    require_once("data/member_dal.php");
    require_once("data/state_dal.php");
    require_once("data/lga_dal.php");
    require_once("data/group_dal.php");
    require_once("data/department_dal.php");
    require_once("data/fellowship_dal.php");
    require_once("data/assembly_dal.php");

    require_once("utilities/utilities.php");

    $status = array();
    $member_id = $_GET['mid'];

    $member = MemberDAL::fetch($member_id);
    
?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>CooperativePro :: View Customer</title>        <meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">         <!--base css styles-->
        <link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/normalize/normalize.css">

        <!--page specific css styles-->
        <link rel="stylesheet" href="assets/prettyPhoto/css/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        
        <link rel="stylesheet" type="text/css" href="assets/data-tables/DT_bootstrap.css" />
        
        <!--flaty css styles-->
        <link rel="stylesheet" href="css/flaty.css">
        <link rel="stylesheet" href="css/flaty-responsive.css">
        
        <link rel="shortcut icon" href="img/favicon.html">
        
        <script src="assets/modernizr/modernizr-2.6.2.min.js"></script>
        
        <!--basic scripts-->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="assets/jquery/jquery-1.10.1.min.js"><\/script>')</script>
        <script src="assets/bootstrap/bootstrap.min.js"></script>
        <script src="assets/nicescroll/jquery.nicescroll.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <!-- BEGIN Brand -->
                    <a href="#" class="brand">
                        <i class="icon-globe"></i>
                        Penticostal Assemblies Management Software :: Member Info
                    </a>
                    <!-- END Brand -->
                </div><!--/.container-fluid-->
            </div><!--/.navbar-inner-->
        </div>
        <!-- END Navbar -->
        
        <!-- BEGIN Container -->
        <div class="container-fluid" id="main-container" style="margin-left:-45px">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="nav-collapse sidebar-collapsed">
            </div>
            <!-- END Sidebar -->
                
                <!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Page Title -->

    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-reorder"></i>  Member Information</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                    </div>
                </div>

                <div class="box-content">
                                <div class="row-fluid">
                                <div class="span6"> 
                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Tithe Card No: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_tithe_card_no(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                First Name 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_first_name(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Middle Name:  
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_middle_name(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Last Name 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_last_name(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Gender 
                                            </div>
                                             <div class="span8">
                                                    <?php echo get_gender($member->get_gender()); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Date Of Birth:
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_date_of_birth(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Occupation:
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_occupation(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Phone Number: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_phone_no(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                E-mail: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_email(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Address: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_address(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                State: 
                                            </div>
                                             <div class="span8">
                                                    <?php 
                                                    
                                                    $state = StateDAL::fetch($member->get_state_id());
                                                    echo $state->get_name();
                                                    ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                LGA: 
                                            </div>
                                             <div class="span8">
                                                    <?php 
                                                        $lga = LGADAL::fetchById($member->get_lga_id());
                                                        echo $lga->get_name();
                                                    ?>

                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Group: 
                                            </div>
                                             <div class="span8">
                                                    <?php 
                                                    $group = GroupDAL::fetch($member->get_group_id()); 
                                                    echo $group->get_name();
                                                    ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Department: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_department_id(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Fellowship: 
                                            </div>
                                             <div class="span8">
                                                    <?php 
                                                    $fellowship = FellowshipDAL::fetch($member->get_fellowship_id());

                                                    echo $fellowship->get_name(); 

                                                    ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Assembly: 
                                            </div>
                                             <div class="span8">
                                                    <?php 
                                                        $assembly = AssemblyDAL::fetch($member->get_assembly_id());

                                                        echo $assembly->get_name(); 
                                                    ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <hr />

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Date Baptized: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_date_baptized(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Date Born-Again: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_date_born_again(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Spouse Name: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_spouse_name(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>

                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Spouse Phone Number: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_spouse_phone_no(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>


                                    <div class="control-group">
                                        <div class="row-fluid">
                                            <div class="span4 controls">
                                                Number of Children: 
                                            </div>
                                             <div class="span8">
                                                    <?php echo $member->get_no_of_children(); ?>
                                             </div>
                                        </div><!--row-fluid-->
                                    </div>
                                </div><!--span6-->
                                    
                                <div class="span6">
                                <div class="row-fluid">
                                   <div class="span12">
                                    <div class="control-group">
                                      <div class="controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                                <img src="<?php echo $member->get_avatar_location() ?>" />


                                            </div>

                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                </div>
                               </div>
                               </div>

                        </div>
                
            </div>
        </div>
    </div>
<!-- END Main Content -->                
                <!--<footer>
                    <p>2016 &copy; Chibex Technologies.</p>
                </footer>-->

                <a id="btn-scrollup" class="btn btn-circle btn-large" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->
        
        <!--page specific plugin scripts-->
        <script src="assets/prettyPhoto/js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="assets/chosen-bootstrap/chosen.jquery.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script type="text/javascript" src="assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="assets/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
        <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-switch/static/js/bootstrap-switch.js"></script>
        <script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
        <script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script> 
        
        <!--page specific plugin scripts-->
        <script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
        <script type="text/javascript" src="assets/jquery-validation/dist/additional-methods.min.js"></script>
     
         <!--flaty scripts-->
        <script src="js/flaty.js"></script>
   </body>
</html>