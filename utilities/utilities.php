<?PHP
	


	function get_gender($gender_code){
		$gender_array = array("0" => "Male", "1" => "Female");
		return $gender_array[$gender_code];
	}

	function get_member_name($member_id){
		require_once("./data/member_dal.php");
		$member = MemberDAL::fetch($member_id);
		return $member->get_last_name() . ", ".$member->get_first_name() ." ".$member->get_middle_name();
	}

	function get_user_name($user_id){
		require_once("./data/user_dal.php");
		$user = UserDAL::fetch($user_id);
		return $user->get_full_name();	
	}

	function get_expenditure_account_name($expenditure_account_id){
		require_once("./data/expenditure_account_dal.php");
		$expenditure_account = ExpenditureAccountDAL::fetch($expenditure_account_id);

		return $expenditure_account->get_name();
	}

	function upload_image($filename, $relpath)
	{
		$passport_url= "";
		if(isset($_FILES[$filename]['tmp_name']) && is_uploaded_file($_FILES[$filename]['tmp_name']))
		{
			$name_ext_arr = preg_split("/\./",$_FILES[$filename]['name']);
			$image_ext = end($name_ext_arr);
			//echo $image_ext;
			if(validate_type($image_ext))
			{
				$new_name = time().rand(0,1000).".".$image_ext;
				move_uploaded_file($_FILES[$filename]['tmp_name'], $relpath.$new_name) or die("could not upload this file");
				$passport_url = $new_name;
			}
		}
		//delete_files("../images/photos/temp");
		return $passport_url;
		
	}
	
	function validate_type($cur_type)
	{
		$image_types = array("gif", "png", "jpeg", "jpg");
		return in_array($cur_type, $image_types);
	}
	
	function delete_files($dir) 
	{
		$dhandle = opendir($dir);
		if ($dhandle) 
		{
			while (false !== ($fname = readdir($dhandle)))
			{
				if(!is_dir("{$dir}/{$fname}"))
					unlink("{$dir}/{$fname}");
			}
			closedir($dhandle);
		}
	}
	
	function format_date_out($date, $time_flag = TRUE)
	{
		$date_arr = explode(' ', $date);
		
		$formated_date = strftime('%d/%m/%Y',strtotime($date_arr[0]));
		
		if(count($date_arr) === 2 && $time_flag)
			$formated_date .= ' '.$date_arr[1];
			
		return $formated_date;
	}
	
	function get_sms_type($type_id)
	{
		global $sms_type_arr;
		foreach($$sms_type_arr as $key => $value)
		{
			if($type_id == $value)
				return $key;
		}
		
		return '';
	}

	$lgas_arr = array('ABI', 'AKAMKPA', 'AKPABUYO', 'BAKASSI', 'BEKWARRA', 'BIASE', 'BOKI', 'CALABAR MUNICIPAL', 'CALABAR SOUTH', 'ETUNG', 'IKOM', 'OBANLIKU', 'OBUBRA', 'OBUDU', 'ODUKPANI', 'OGOJA', 'YAKURR', 'YALA', 'NON-CROSSRIVERIAN');
?>