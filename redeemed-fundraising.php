<?php 
    session_start();
    require_once("functions.php");
    require_once("data/redeemed_pledge_dal.php");
    require_once("data/fund_raising_type_dal.php");
    $status = array();
    $pledge = null;

    if(!isset($_SESSION['user_id'])){
        header("Location: index.php");
        exit();
    }

    if(isset($_POST['submit']) || isset($_POST['update']))
    {
        if(isset($_POST['txtAmount']) && !empty($_POST['txtAmount']) && isset($_POST['txtDate']) && !empty($_POST['txtDate']) && isset($_POST['downFundRaisingId']) && !empty($_POST['downFundRaisingId']))
        {
            $amount = strip_tags($_POST['txtAmount']);
            $date = strip_tags($_POST['txtDate']);
            $pledge_id  = strip_tags($_POST['downFundRaisingId']);

            $pledge = new RedeemedPledge();
            $pledge->set_pledge_id($pledge_id);
            $pledge->set_amount($amount);
            $pledge->set_date($date);
            $pledge->set_user_id($_SESSION['user_id']);
            $pledge->set_date_created(date('Y-m-d H:i:s'));

            $pledge_model = new RedeemedPledgeDAL($pledge);
            

            if(isset($_POST['submit'])){
                $flag = $pledge_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your pledge was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }
            }else {
                $pledge->set_id($_GET['redeemed_fund_raising_id']);
                $flag = $pledge_model->update();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your pledge was updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Updating Data. Try again';
                }
            }

            

        }
    }

    if(isset($_GET['redeemed_fund_raising_id'])){
        $pledge = RedeemedPledgeDAL::fetch($_GET['redeemed_fund_raising_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Redeemed Pledges';
    include('header.php');
    include('menu.php');
    
    display_menu(5, 2);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Redeemed Fundraising</h1>
                        <h4>Form Redeemed Fundraising</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Redeemed Fundraising</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i>Redeemed Fundraising Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal"  method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="downFundRaisingId">Fundraising Id</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name='downFundRaisingId'>
                                               
                                               <?php 
                                                    $fund_raising_type_id = ($pledge != null) ? $pledge->get_pledge_id() : '0';

                                                    $fund_raising_types = FundraisingTypeDAL::fetch_all();
                                                    foreach($fund_raising_types as $fund_raising_type){
                                                        echo '<option value="'.$fund_raising_type->id.'" ';
                                                        echo ($fund_raising_type_id == $fund_raising_type->id) ? 'selected' : '';
                                                        echo '>'.$fund_raising_type->name.'</option>';
                                                    }
                                               ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" value="<?php echo ($pledge != null) ? $pledge->get_amount() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDate" id="txtDate" value="<?php echo ($pledge != null) ? $pledge->get_date() : date('d/m/Y', time()); ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['redeemed_fund_raising_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['redeemed_fund_raising_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>