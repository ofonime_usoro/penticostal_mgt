<?php
    session_start(); 
    require_once("data/department.php");
    require_once("data/department_dal.php");

    $status = array();
    $department = null; 

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    if(isset($_POST['update']) || isset($_POST['submit'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName'])){
            $name = strip_tags($_POST['txtName']);
            $description = strip_tags($_POST['txtDescription']);

            $department = new Department();
            $department->set_name($name);
            $department->set_description($description);

            $department_model = new DepartmentDAL($department);


            if(isset($_POST['submit'])){

                $flag = $department_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Department was added successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering department. Try again';
                }

            }else {
                $department->set_id($_GET['department_id']);
                $flag = $department_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Department updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating department. Try again';
                }  
            }
        }
    }

    if(isset($_GET['department_id'])){
        $department = DepartmentDAL::fetch($_GET['department_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(1, 6);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Department</h1>
                        <h4>View of all the Department of the penticostal</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Department</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i> Department</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="POST">
                                    <div class="control-group">
                                        <label for="txtName" class="control-label">Name</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtName" id="txtName" data-rule-maxlength="50" data-rule-required="true"
                                            value="<?php echo ($department!= null) ? $department->get_name() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" id="txtDescription" placeholder="Enter Description"><?php echo ($department!= null) ? $department->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>     
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['department_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['department_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>