<?php
    session_start();

    require_once("data/tithe.php");
    require_once("data/tithe_dal.php");
    require_once("functions.php");
    $status = array();
    $tithe = null;


    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    if(isset($_GET['tithe_id'])){
        $tithe_id = strip_tags($_GET['tithe_id']);
        $tithe = TitheDAL::fetch($tithe_id);
    }

    if(isset($_POST['update'])){
        if(isset($_POST["txtFullName"]) && !empty($_POST["txtFullName"]) && isset($_POST["txtCardNumber"]) && !empty($_POST["txtCardNumber"]) && isset($_POST["txtWeekNo"]) && !empty($_POST["txtWeekNo"]) && isset($_POST["txtAmount"]) && !empty($_POST["txtAmount"]) && isset($_POST["txtDate"])){
            $card_no = strip_tags($_POST["txtCardNumber"]);
            $week_no = strip_tags($_POST["txtWeekNo"]);
            $amount = strip_tags($_POST['txtAmount']);
            $date = strip_tags($_POST["txtDate"]);
            $user_id = $_SESSION['user_id'];

            $updated_tithe = new Tithe();
            $updated_tithe->set_card_no($card_no);
            $updated_tithe->set_week_no($week_no);
            $updated_tithe->set_amount($amount);
            $updated_tithe->set_date($date);
            $updated_tithe->set_user_id($user_id);
            $updated_tithe->set_member_id($tithe->get_member_id());
            $updated_tithe->set_date_created($tithe->get_date_created());

            $flag = TitheDAL::update($updated_tithe, $tithe_id);
            if($flag == 1)
            {
                $status['style'] = 'alert-success';
                $status['title'] = 'Success';
                $status['message'] = 'Your tithe was updated successfully!';
            }
            else if($flag == 0)
            {
                $status['style'] = 'alert-error';
                $status['title'] = 'Error';
                $status['message'] = 'Error Inserting Data. Try again';
            }
        }
    }


    if(isset($_POST["submit"])){
        if(isset($_POST["txtFullName"]) && !empty($_POST["txtFullName"]) && isset($_POST["txtCardNumber"]) && !empty($_POST["txtCardNumber"]) && isset($_POST["txtWeekNo"]) && !empty($_POST["txtWeekNo"]) && isset($_POST["txtAmount"]) && !empty($_POST["txtAmount"]) && isset($_POST["txtDate"])){
            $card_number = strip_tags($_POST["txtCardNumber"]);
            $week_no = strip_tags($_POST["txtWeekNo"]);
            $amount = strip_tags($_POST["txtAmount"]);
            $full_name = strip_tags($_POST["txtFullName"]);
            $date = strip_tags($_POST["txtDate"]);

            $date = explode("/", $date);
            $date = $date[2]. "-".$date[1]."-".$date[0];

            $tithe = new Tithe();
            $tithe->set_member_id($_POST["txtMemberId"]);
            $tithe->set_card_no($card_number);
            $tithe->set_amount($amount);
            $tithe->set_week_no($week_no);
            $tithe->set_user_id($_SESSION["user_id"]);
            $tithe->set_date($date);
            $tithe->set_date_created(date("Y-m-d"));

            $tithe_model = new TitheDAL($tithe);
            $flag = $tithe_model->insert();

            if($flag == 1)
            {
                $status['style'] = 'alert-success';
                $status['title'] = 'Success';
                $status['message'] = 'Your Tithe was saved successfully!';
            }
            else if($flag == 0)
            {
                $status['style'] = 'alert-error';
                $status['title'] = 'Error';
                $status['message'] = 'Error Inserting Tithe. Try again';
            }
        }
    }
    
    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(2, 0);

?>

                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Tithe</h1>
                        <!--h4>Use this form to register new member</h4-->
                    </div>
                </div>
                <!-- END Page title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Tithe</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <form action="" class="form-horizontal" method="post" enctype="mutipart/form-data">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Tithe</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            	<div class="row-fluid">
                            	<div class="span6">

                                    <div class="control-group">
                                        <div id="error"></div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtCardNumber" class="control-label">Card Number</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtCardNumber" id="txtCardNumber" data-rule-digits="true" data-rule-required="true" 
                                                value="<?php echo ($tithe != null) ? $tithe->get_card_no() : ''; ?>" 
                                            >

                                            <!--<span class="error">Enter a Valid Card Number</span>-->
                                        </div>
                                        

                                    </div>

                                    <div class="control-group">
                                        <label for="txtFullName" class="control-label">Full Name</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" readonly name="txtFullName" id="txtFullName" data-rule-digits="true" data-rule-required="true"
                                            value="<?php echo ($tithe != null) ? get_member_fullname($tithe->get_member_id()) : ''; ?>" 
                                            >
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" value="<?php echo ($tithe != null) ? $tithe->get_amount() : ''; ?>" >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtWeekNo" class="control-label">Week No</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only numbers" name="txtWeekNo" id="txtWeekNo" data-rule-number="true" data-rule-required="true" value="<?php echo ($tithe != null) ? $tithe->get_week_no() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDate" value="<?php echo ($tithe != null) ? $tithe->get_date() : date('d/m/Y', time()); ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                         </div>
                                        </div>
                                    </div>
                                 </div>
                                    
                                <div class="span6">
                                <div class="row-fluid">
                               	   <div class="span12">
                                    <div class="control-group">
                                      <div class="controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                               <img id="imgAvatar" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                               <span class="btn btn-file"><span class="fileupload-new">Choose passport</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default" readonly /></span>
                                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                </div>
                               </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="hidden" name="txtMemberId" id="memberId"><input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['tithe_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['tithe_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                    <button type="button" class="btn">Cancel</button>
                </div>
                
                </form>
                <!-- END Main Content -->
                <?php include('footer.php');?>
                <script type="text/javascript">
                    
                    $(document).ready(function(){
                        $("#txtCardNumber").focusout(function(){
                            console.log($(this).val());
                            var postForm = {
                                "card_no" : $(this).val()
                            };
                            $.ajax({
                                type : 'POST',
                                url : 'request.php',
                                data : postForm,
                                dataType: 'json', 
                                success : function(data){
                                    //$(this).attr("#txtCardNumber")
                                    var avatar_url = "uploads";
                                    if(data.status === "success"){
                                        $("#txtFullName").val(data.full_name);
                                        $("#imgAvatar").attr("src", avatar_url+"/"+data.img_url);
                                        $("#memberId").attr("value", data.id);

                                    }else if(data.status === "failure"){
                                        $("#error").html("Invalid Card Number");
                                    }
                                    

                                    
                                }
                            })
                        });
                    })
                </script>