<?php 
    session_start();
    require_once("data/member.php");
    require_once("data/member_dal.php");
    require_once("data/state_dal.php");
    require_once("data/lga_dal.php");
    require_once("data/group_dal.php");
    require_once("data/department_dal.php");
    require_once("data/fellowship_dal.php");
    require_once("data/assembly_dal.php");
    require_once("utilities/utilities.php");

    $status = array();

    $member = null;

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    if(isset($_POST['update']) || isset($_POST['submit'])){    
        if(isset($_POST['txtFirstName']) && !empty($_POST['txtFirstName']) && isset($_POST['txtMiddleName']) && !empty($_POST['txtMiddleName']) && isset($_POST['txtLastName']) && !empty($_POST['txtLastName']) && isset($_POST['txtGender']) && isset($_POST['txtDateOfBirth']) && !empty($_POST['txtDateOfBirth']) && isset($_POST['txtOccupation']) && !empty($_POST['txtOccupation']) && isset($_POST['txtPhoneNo']) && !empty($_POST['txtPhoneNo']) && isset($_POST['txtEmail']) && !empty($_POST['txtEmail']) && isset($_POST['txtAddress']) && !empty($_POST['txtAddress']) && isset($_POST['downState']) && !empty($_POST['downState']) && isset($_POST['downLga']) && !empty($_POST['downLga']) && isset($_POST['downGroup']) && !empty($_POST['downGroup']) && isset($_POST['downDepartment']) && !empty($_POST['downDepartment']) && isset($_POST['downFellowship']) && !empty($_POST['downFellowship']) && isset($_POST['downAssembly']) && !empty($_POST['downAssembly'])){
            $member = new Member();
            $member->set_tithe_card_no($_POST['txtTitheCardNo']);
            $member->set_first_name($_POST['txtFirstName']);
            $member->set_middle_name($_POST['txtMiddleName']);
            $member->set_last_name($_POST['txtLastName']);
            $member->set_gender($_POST['txtGender']);
            $member->set_occupation($_POST['txtOccupation']);
            $member->set_phone_no($_POST['txtPhoneNo']);
            $member->set_email($_POST['txtEmail']);
            $member->set_address($_POST['txtAddress']);
            $member->set_state_id($_POST['downState']);
            $member->set_lga_id($_POST['downLga']);
            $member->set_group_id($_POST['downGroup']);
            $member->set_department_id($_POST['downDepartment']);
            $member->set_fellowship_id($_POST['downFellowship']);
            $member->set_assembly_id($_POST['downAssembly']);
            $member->set_spouse_name($_POST['txtSpouseName']);
            $member->set_spouse_phone_no($_POST['txtSpousePhoneNo']);
            $member->set_no_of_children($_POST['txtNoOfChildren']);

            $date_baptized = explode("/", $_POST['txtDateBaptized']);
            $date_baptized = $date_baptized[2]. "-".$date_baptized[1]."-".$date_baptized[0];
            $member->set_date_baptized($date_baptized);

            $date_born_again = explode("/", $_POST['txtDateBornAgain']);
            $date_born_again = $date_born_again[2]. "-".$date_born_again[1]."-".$date_born_again[0];
            $member->set_date_born_again($date_born_again);
           
            $date_of_birth = explode("/", $_POST['txtDateOfBirth']);
            $date_of_birth = $date_of_birth[2]. "-".$date_of_birth[1]."-".$date_of_birth[0];
            $member->set_date_of_birth($date_of_birth);

            $avatar_location = !empty($_FILES['avatar_name']) ? upload_image('avatar_name', 'uploads/') : '';

            $member->set_avatar_location($avatar_location);


            $member_model = new MemberDAL($member);

            if(isset($_POST['submit'])){

                $flag = $member_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Member was registered successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering member. Try again';
                }

            }else {
                $member->set_id($_GET['edit_member']);
                $flag = $member_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Member updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating member. Try again';
                }  
            }
        }
    }

    if(isset($_GET['edit_member'])){
        $member = MemberDAL::fetch(intval($_GET['edit_member']));
    }

    header('Content-Type: text/html');
    $page_title = 'Membership Form';
    include('header.php');
    include('menu.php');
    
    display_menu(1, 0);
    
?>

                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Membership Form</h1>
                        <h4>Use this form to register new member</h4>
                    </div>
                </div>
                <!-- END Page title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Membership Form</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Membership Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            	<div class="row-fluid">
                            	<div class="span6">
                                    <div class="control-group">
                                        <label class="control-label" for="txtTitheCardNo">Tithe Card No:</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtTitheCardNo" id="txtTitheCardNo" class="input-xlarge" data-rule-required="true" data-rule-maxlength="50" 
                                                value="<?php echo ($member != null) ? $member->get_tithe_card_no() : ''; ?>" 
                                                > 
                                                <img class="ajax-image" />
                                                <span class="icon-image"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtFirstName">First Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtFirstName" id="txtFirstName" class="input-xlarge" data-rule-required="true" data-rule-maxlength="50" 
                                                value="<?php echo ($member != null) ? $member->get_first_name() : ''; ?>" 
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtMiddleName">Middle Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtMiddleName" id="txtMiddleName" class="input-xlarge" data-rule-required="true" data-rule-maxlength="50" 
                                                value="<?php echo ($member != null) ? $member->get_middle_name() : ''; ?>" 
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtLastName">Last Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtLastName" id="txtLastName" class="input-xlarge" data-rule-required="true" data-rule-maxlength="50" 
                                                value="<?php echo ($member != null) ? $member->get_last_name() : ''; ?>"  
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                      <label class="control-label" for="txtGender">Gender</label>
                                      <div class="controls">
                                         <label class="radio inline">
                                            <input type='radio' value='0' 
                                                <?php 
                                                    if($member != null){
                                                        if($member->get_gender() == '0') 
                                                            echo "checked";
                                                    }
                                                ?>
                                            name="txtGender">Male

                                         </label>

                                         <label class="radio inline">
                                            <input type='radio' value='1' 
                                                <?php 
                                                    if($member != null){
                                                        if($member->get_gender() == '1') 
                                                            echo "checked";
                                                    }
                                                ?>
                                            name="txtGender">Female   
                                         </label>
                                      </div>
                                   </div>
                                   <div class="control-group">
                                      <label class="control-label" for="txtDateOfBirth">Date of Birth</label>
                                      <div class="controls">
                                         <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDateOfBirth" value="<?php echo date('d/m/Y', time()) ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                         </div>
                                      </div>
                                   </div>
                                   <div class="control-group">
                                        <label for="txtOccupation" class="control-label">Occupation</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtOccupation" id="txtOccupation" data-rule-maxlength="50" data-rule-required="true"
                                            value="<?php echo ($member != null) ? $member->get_occupation() : ''; ?>"
                                            >

                                        </div>
                                    </div>
                                   <div class="control-group">
                                        <label for="txtPhoneNo" class="control-label">Phone No</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtPhoneNo" id="txtPhoneNo" data-rule-digits="true" data-rule-required="true"
                                            value="<?php echo ($member != null) ? $member->get_phone_no() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="txtEmail">Email Address</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="email" name="txtEmail" id="txtEmail" class="input-xlarge" data-rule-required="true" data-rule-email="true" 
                                                value="<?php echo ($member != null) ? $member->get_email() : ''; ?>"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAddress" class="control-label">Address</label>
                                        <div class="controls">
                                            <textarea type="text" class="input-xlarge" name="txtAddress" id="txtAddress" data-rule-required="true"><?php echo ($member != null) ? $member->get_address() : ''; ?></textarea>
                                        </div>
                                    </div>
                                 </div>
                                    
                                <div class="span6">
                                <div class="row-fluid">
                               	   <div class="span12">
                                    <div class="control-group">
                                      <div class="controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                                <img src="<?php echo ($member != null) ? 'uploads/'.$member->get_avatar_location() : ''; ?>" />

                                            </div>

                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                               <span class="btn btn-file"><span class="fileupload-new">Choose passport</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default" name="avatar_name" id="avatar_name" /></span>
                                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                </div>
                               </div>
                               </div>
                               <div class="row-fluid">
                               	   <div class="span12">
                                    <hr>

                                    <div class="control-group">
                                        <label for="downState" class="control-label">State</label>
                                        <div class="controls">
                                            <select name='downState' id='downState' >
                                            <?php 
                                                $state_id = ($member != null) ? $member->get_state_id() : 0;
                                                $states = StateDAL::fetch_all();

                                                foreach($states as $state){
                                                    echo '<option value="'.$state->id.'" ';
                                                    echo ($state_id == $state->id) ? 'selected' : '';
                                                    echo '>'.$state->name.'</option>';
                                                }


                                            ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label for="downLga" class="control-label">LGA</label>
                                        <div class="controls">
                                            <select name="downLga" id="downLga">
                                            <?php 
                                                $lga_id = ($member != null) ? $member->get_lga_id() : 0;

                                                $lgas = LGADAL::fetch_all($state_id);

                                                foreach($lgas as $lga){
                                                    echo '<option value="'.$lga->id.'" ';
                                                    echo ($lga_id == $lga->id) ? 'selected' : '';
                                                    echo '>'.$lga->name.'</option>';
                                                }


                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <hr>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="downGroup">Group</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name='downGroup'>

                                                <?php 
                                                    $group_id = ($member != null) ? $member->get_group_id() : '0';
                                                    $groups = GroupDAL::fetch_all();

                                                    foreach($groups as $group){
                                                        echo '<option value="'.$group->id.'" ';
                                                        echo ($group_id == $group->id) ? 'selected' : '';
                                                        echo '>'.$group->name.'</option>';
                                                    }
                                                    
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="downDepartment">Department</label>
                                        <div class="controls">
                                            <div class="span12">
                                               <select name='downDepartment'>
                                               
                                               <?php 
                                                    $department_id = ($member != null) ? $member->get_department_id() : '0';

                                                    $departments = DepartmentDAL::fetch_all();
                                                    foreach($departments as $department){
                                                        echo '<option value="'.$department->id.'" ';
                                                        echo ($department_id == $department->id) ? 'selected' : '';
                                                        echo '>'.$department->name.'</option>';
                                                    }
                                               ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="downFellowship">Fellowship</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name='downFellowship'>
                                               
                                               <?php 
                                                    $fellowship_id = ($member != null) ? $member->get_fellowship_id() : '0';

                                                    $fellowships = FellowshipDAL::fetch_all();
                                                    foreach($fellowships as $fellowship){
                                                        echo '<option value="'.$fellowship->id.'" ';
                                                        echo ($fellowship_id == $fellowship->id) ? 'selected' : '';
                                                        echo '>'.$fellowship->name.'</option>';
                                                    }
                                               ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="downAssembly">Assembly</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name='downAssembly'>
                                               
                                               <?php 
                                                    $assembly_id = ($member != null) ? $member->get_assembly_id() : '0';

                                                    $assemblies = AssemblyDAL::fetch_all();
                                                    foreach($assemblies as $assembly){
                                                        echo '<option value="'.$assembly->id.'" ';
                                                        echo ($assembly_id == $assembly->id) ? 'selected' : '';
                                                        echo '>'.$assembly->name.'</option>';
                                                    }
                                               ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="control-group">
                                        <label for="txtDateBaptized" class="control-label">Date Baptised</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDateBaptized" id="txtDateBaptized" value="<?php if(isset($current_member)) echo $current_member->date_of_birth; else echo date('d/m/Y', time()); ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                         </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDateBornAgain" class="control-label">Date Born Again</label>
                                        <div class="controls">

                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDateBornAgain" id="txtDateBornAgain" value="<?php echo date('d/m/Y', time()) ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                         </div>
                                        </div>
                                    </div>
                                   
                                   <div class="control-group">
                                        <label class="control-label" for="txtSpouseName">Spouse Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtSpouseName" id="txtSpouseName" class="input-xlarge" data-rule-required="true" data-rule-maxlength="50" value="<?php echo ($member != null) ? $member->get_spouse_name() : ''; ?>"
                                                 />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtSpousePhoneNo" class="control-label">Spouse Phone No</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only numbers" name="txtSpousePhoneNo" id="txtSpousePhoneNo" data-rule-number="true" data-rule-required="true" value="<?php echo ($member != null) ? $member->get_spouse_phone_no() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtNoOfChildren" class="control-label">No Of Children</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtNoOfChildren" id="txtNoOfChildren" data-rule-digits="true" data-rule-required="true" value="<?php echo ($member != null) ? $member->get_no_of_children() : ''; ?>">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['edit_member'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['edit_member'])) { echo 'update'; }else { echo 'submit'; } ?>">
                    <button type="reset" class="btn">Cancel</button>
                </div>
                
                </form>
                <!-- END Main Content -->
                <?php include('footer.php');?>

                <script type="text/javascript">
                    
                    $(document).ready(function(){

                        $("#downState").change(function(){
                            //alert($(this).val());
                            $("#downLga").html("<option value='#'>Loading...</option>");
                            var lgas = '';

                            var postForm = {
                                "state_id" : $(this).val()
                            };

                            $.ajax({
                                type: 'POST',
                                url : 'request.php',
                                data: postForm,
                                dataType: 'json',
                                success : function(data){
                                    $.each(data, function(key, value){
                                        lgas += "<option value='"+data[key].id+"'>"+data[key].name+"</option>";
                                    });

                                    $("#downLga").html(lgas);
                                }
                            })
                        });

                        $('#txtTitheCardNo').focusout(function(){
                            //alert("I focused Out");
                            if(!$(this).val()){
                                alert("I am Empty");
                                console.log("I am Empty");
                                return;
                            }else {
                                console.log("I am filled");
                                $('.ajax-image').show();
                                $('.ajax-image').attr('src', "img/ajax_loading.gif");
                                var postForm = {
                                "tithe_card_no" : $("#txtTitheCardNo").val()
                                };

                                $.ajax({
                                    type : 'POST',
                                    url : 'request.php',
                                    data : postForm,
                                    dataType: 'json', 
                                    success : function(data){
                                        $('.ajax-image').hide();
                                        if(data.status === "success"){
                                            //console.log("This Tithe Number is available for use");
                                            $('.icon-image').html('<i class="icon-ok" style="color: green"></i>');
                                        }else if(data.status === "failure"){
                                            //console.log("This Number is already in use");
                                            $('.icon-image').html('<i class="icon-remove" style="color: red"></i>');
                                        }
                                    }
                                })
                            }
                        });
                    });
                </script>
               
