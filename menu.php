<?PHP
//include_once(dirname(__FILE__) .'/../helper/utility.php');
//$utility = new Utility();
	
	$side_menu = array(array('name' => 'Dashboard', 'url' => 'index.php', 'icon' => 'icon-dashboard', 'sub_menu' => array()),
		array('name' => 'Membership', 'icon' => 'icon-user', 'url' => '#', 
				'sub_menu' => array(array('name' => 'Member Registration', 'url' => 'member.php'),
									array('name' => 'Members View', 'url' => 'view_members.php'),
									array('name' => 'Assembly Registration', 'url' => 'assembly.php'),
									array('name' => 'Assembly View', 'url' => 'view_assemblies.php'),
									array('name' => 'Group Registration', 'url' => 'group.php'),
									array('name' => 'Group View', 'url' => 'view_groups.php'),
									array('name' => 'Department Registration', 'url' => 'department.php'),
									array('name' => 'Department View', 'url' => 'view_departments.php'),
									array('name' => 'Fellowship Registration', 'url' => 'fellowship.php'),
									array('name' => 'Fellowship View', 'url' => 'view_fellowship.php'))),
									
		array('name' => 'Tithe', 'icon' => 'icon-file', 'url' => '#', 
				'sub_menu' => array(array('name' => 'Tithe', 'url' => 'tithe.php'),
									array('name' => 'View Tithes', 'url' => 'view_tithes.php'))),
		array('name' => 'Offertory', 'icon' => 'icon-gear', 'url' => '#', 
			'sub_menu' => array(
					array('name' => 'Offering Type', 'url' => 'offering-type.php'),
					array('name' => 'Offering', 'url' => 'offering.php'),
					array('name' => 'View Offering', 'url' => 'view_offering.php'),
					array('name' => 'View Offering Type', 'url' => 'view_offering_type.php'),
			)),
		array('name' => 'Expenditure', 'icon' => 'icon-share', 'url' => '#', 
			'sub_menu' => array(
					array('name' => 'Expenditure Account', 'url' => 'expenditure-account.php'),
					array('name' => 'Expenditure', 'url' => 'expenditure.php'),
					array('name' => 'View Expenditure', 'url' => 'view_expenditure.php'), 
					array('name' => 'View Expenditure Account', 'url' => 'view_expenditure_accounts.php') 

			)),
		array('name' => 'Fund Raising', 'icon' => 'icon-exchange', 'url' => '#', 
			'sub_menu' => array(
					array('name' => 'Fund Raising Type', 'url' => 'fund-raising-type.php'),
					array('name' => 'Fund Raising', 'url' => 'fund-raising.php'),
					array('name' => 'Redeemed Fund Raising', 'url' => 'redeemed-fundraising.php'),
					array('name' => 'View Fund Raising Type', 'url' => 'view_fund_raising_type.php'),
					array('name' => 'View Fund Raising', 'url' => 'view_fund_raising.php'),
					array('name' => 'View Redeemed Fund Raising', 'url' => 'view_redeemed_fundraising.php')
			)),
		array('name' => 'User', 'icon' => 'icon-user', 'url' => '#', 
			'sub_menu' => array(
				array('name' => 'User Registration', 'url' => 'user.php'),
				array('name' => 'View Users', 'url' => 'view_users.php')
			)),

		array('name' => 'Other Income', 'icon' => 'icon-edit', 'url' => '#', 
			'sub_menu' => array(
				array('name' => 'Other Income Type', 'url' => 'other-income-type.php'),
				array('name' => 'Other Income', 'url' => 'other-income.php'),
				array('name' => 'Other Income Account', 'url' => 'other-income-account.php'),
				array('name' => 'View Other Income', 'url' => 'view_other_income.php'),
				array('name' => 'View Other Income Type', 'url' => 'view_other_income_type.php'),
				array('name' => 'View Other Income Account', 'url' => 'view_other_income_account.php'),



			)),

		array('name' => 'Report', 'icon' => 'icon-list-alt', 'url' => '#', 
			'sub_menu' => array(
				array('name' => 'General Ledger', 'url' => 'general_ledger.php'),
				array('name' => 'Balance Sheet', 'url' => 'balance_sheet.php'),
				array('name' => 'Profit and Loss', 'url' => 'profit_loss.php')
			)),

		array('name' => 'Analysis', 'icon' => 'icon-text-width', 'url' => '#', 
			'sub_menu' => array(
				array('name' => 'General Ledger', 'url' => 'general_ledger.php'),
				array('name' => 'Balance Sheet', 'url' => 'balance_sheet.php'),
				array('name' => 'Profit and Loss', 'url' => 'profit_loss.php')
			)),

		array('name' => 'Bulk SMS', 'icon' => 'icon-signal', 'url' => '#', 
			'sub_menu' => array(
				array('name' => 'Members Account Balance', 'url' => 'account-balances.php'),
				array('name' => 'Pledge', 'url' => 'pledge_report.php'),
				array('name' => 'Account Transfer', 'url' => 'account-transfer-report.php'),
				array('name' => 'Marketers Performance', 'url' => 'profit_loss.php')
			)),


	);
	
	function display_menu($active_menu = 0, $active_submenu = 0)
	{
		global $side_menu;
		global $utility;
		$str_menu = '';
		for($x = 0; $x < count($side_menu); $x++)
		{
			if($side_menu[$x]['name'] == 'Affiliate' && isset($_SESSION['is_affiliate']) && !$_SESSION['is_affiliate'])
				continue;
				
			if($side_menu[$x]['name'] == 'Admin Panel' && !in_array($_SESSION['user_name'], $utility->get_admins2()))
				continue;
				
			$str_menu .= '<li '.(($active_menu == $x) ? 'class="active"':'').'>
                        <a href="'.$side_menu[$x]['url'].'" class="dropdown-toggle">
                            <i class="'.$side_menu[$x]['icon'].'"></i>
                            <span>'.$side_menu[$x]['name'].'</span>';
							
							if(count($side_menu[$x]['sub_menu']))
               					$str_menu .= '<b class="arrow icon-angle-right"></b>';
               				$str_menu .= '</a>';
						
					if(count($side_menu[$x]['sub_menu']))
					{
                       $str_menu .= '<ul class="submenu">';
						for($y = 0; $y < count($side_menu[$x]['sub_menu']); $y++)
						{
                            $str_menu .= '<li '.(($active_menu == $x && $active_submenu == $y) ? 'class="active"':'').'>
							<a href="'.$side_menu[$x]['sub_menu'][$y]['url'].'">'.$side_menu[$x]['sub_menu'][$y]['name'].'</a>
							</li>';
						}	
                       $str_menu .= '</ul>';
					}
						
          	$str_menu .= '</li>';
		}
		echo $str_menu;
	}