<?php 
    session_start();
    require_once("data/other_income.php");
    require_once("data/other_income_dal.php");
    require_once("data/other_income_type_dal.php");
    $status = array();
    $other_income = null;

    if(!isset($_SESSION["user_id"])){
        header("Location: login.php");
        exit();
    }
    
    include_once("functions.php");

    if(isset($_POST["submit"]) || isset($_POST['update']))
    {
        if(isset($_POST['txtAmount']) && !empty($_POST["txtAmount"]) && isset($_POST["downIncomeTypeId"]) && isset($_POST["txtDate"]) && !empty($_POST["txtDate"])){
            $type_id = strip_tags($_POST["downIncomeTypeId"]);
            $date = strip_tags($_POST["txtDate"]);
            $amount = strip_tags($_POST["txtAmount"]);
            $description = strip_tags($_POST["txtDescription"]);

            $other_income = new OtherIncome();
            //$other_income->set_id(1);
            $other_income->set_type_id(intval($type_id));
            $other_income->set_amount($amount);

           // $date = explode("/", $date);
            //$date = $date[2]. "-".$date[1]."-".$date[0];
            $other_income->set_date($date);
            $other_income->set_user_id(intval($_SESSION["user_id"]));
            $other_income->set_description($description);
            $other_income->set_date_created(date("Y-m-d H:i:s"));

            $other_income_model = new OtherIncomeDAL($other_income);

            if(isset($_POST['submit'])){
                $flag = $other_income_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Other Income was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }

            }else {
                $other_income->set_id($_GET['other_income_id']);
                $flag = $other_income_model->update();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Other Income was updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }

            }
            

            
        }
    }

    if(isset($_GET['other_income_id'])){
        $other_income = OtherIncomeDAL::fetch($_GET['other_income_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(7, 1);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Other Income</h1>
                        <h4>Form Other Income</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Other Income</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Other Income Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="#" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="downIncomeTypeId">Type Id</label>
                                        <div class="controls">
                                            <select name='downIncomeTypeId'>
                                               
                                               <?php 
                                                    $income_type_id = ($other_income != null) ? $other_income->get_type_id() : '0';

                                                    $other_incomes = OtherIncomeTypeDAL::fetch_all();
                                                    foreach($other_incomes as $incomes){
                                                        echo '<option value="'.$incomes->id.'" ';
                                                        echo ($income_type_id == $incomes->id) ? 'selected' : '';
                                                        echo '>'.$incomes->name.'</option>';
                                                    }
                                               ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" value="<?php echo ($other_income != null) ? $other_income->get_amount() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="YYYY-MM-DD" name="txtDate" id="txtDate" data-rule-dateISO="true" data-rule-required="true" value="<?php echo ($other_income != null) ? $other_income->get_date() : date('d/m/Y', time()); ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" placeholder="Enter Description"><?php echo ($other_income != null) ? $other_income->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>   

                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['other_income_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['other_income_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>