<?php 
    session_start();
    require_once("functions.php");
    require_once("data/fund_raising.php");
    require_once("data/fund_raising_dal.php");

    $status = array();
    $fund_raising = null;

    if(!isset($_SESSION['user_id'])){
        header("Location: index.php");
        exit();
    }

    if(isset($_POST['submit']) || isset($_POST['update'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName']) && isset($_POST['txtAmount']) && !empty($_POST['txtAmount']) && isset($_POST['txtAmountRedeemed']) && !empty($_POST['txtAmountRedeemed']) && isset($_POST['txtDate']) && !empty($_POST['txtDate']) && isset($_POST['txtPhoneNo']) && !empty($_POST['txtPhoneNo']) && isset($_POST['txtAddress']) && !empty($_POST['txtAddress']) && isset($_POST['downFundRaisingId']) && !empty($_POST['downFundRaisingId'])){
            $fund_raising_id = strip_tags($_POST['downFundRaisingId']);
            $name = strip_tags($_POST['txtName']);
            $amount = strip_tags($_POST['txtAmount']);
            $amountRedeemed = strip_tags($_POST['txtAmountRedeemed']);
            $date = strip_tags($_POST['txtDate']);
            $phone_number = strip_tags($_POST['txtPhoneNo']);
            $address = strip_tags($_POST['txtAddress']);

            $fund_raising = new FundRaising();
            $fund_raising->set_fund_raising_id(intval($fund_raising_id));
            $fund_raising->set_user_id($_SESSION['user_id']);
            $fund_raising->set_name($name);
            $fund_raising->set_amount($amount);
            $fund_raising->set_amount_redeemed($amountRedeemed);

            //$date = explode("/", $date);
            //$date = $date[2]. "-".$date[1]."-".$date[0];
            $fund_raising->set_date($date);
            $fund_raising->set_date_created(date('Y-m-d H:i:s'));
            $fund_raising->set_phone_no($phone_number);
            $fund_raising->set_address($address);

            $fund_raising_model = new FundRaisingDAL($fund_raising);
            

            if(isset($_POST['submit'])){
                $flag = $fund_raising_model->insert();
                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Fund Raising was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Data. Try again';
                }

            }else {
                $fund_raising->set_id($_GET['fund_raising_id']);
                $flag = $fund_raising_model->update();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your Fund Raising was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Fund raising data. Try again';
                }
            }

            

        }
        
    }

    if(isset($_GET['fund_raising_id'])){
        $fund_raising = FundRaisingDAL::fetch($_GET['fund_raising_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(5, 1);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Fund Raising</h1>
                        <h4>Form Fund Raising sample</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Fund Raising</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <!--form action="" class="form-horizontal" id="validation-form" method="post"-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i>Fund Raising Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="downFundRaisingId">Fundraising Id</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <?php get_fund_raising_type_dropdown(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" value="<?php echo ($fund_raising != null) ? $fund_raising->get_amount() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmountRedeemed" class="control-label">Amount Redeemed</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmountRedeemed" id="txtAmountRedeemed" data-rule-digits="true" data-rule-required="true" value="<?php echo ($fund_raising != null) ? $fund_raising->get_amount_redeemed() : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDate" id="txtDate" value="<?php echo ($fund_raising != null) ? $fund_raising->get_date() : date('d/m/Y', time()) ?>" /><!--<span class="add-on"><i class="icon-calendar"></i></span>-->
                                           </div>
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="txtName">Name</label>
                                        <div class="controls">
                                            <input type="text" name="txtName" id="txtName" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" value="<?php echo ($fund_raising != null) ? $fund_raising->get_name() : ''; ?>"/>
                                         </div>
                                     </div>
                                     <div class="control-group">
                                        <label for="txtPhoneNo" class="control-label">Phone No</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtPhoneNo" id="txtPhoneNo" data-rule-digits="true" data-rule-required="true"
                                            value="<?php echo ($fund_raising != null) ? $fund_raising->get_phone_no() : ''; ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAddress" class="control-label">Address</label>
                                        <div class="controls">
                                            <textarea type="text" class="input-xlarge" name="txtAddress" id="txtAddress" data-rule-required="true"><?php echo ($fund_raising != null) ? $fund_raising->get_address() : ''; ?></textarea>
                                        </div>
                                    </div>  
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['fund_raising_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['fund_raising_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>
