<?php 
	require_once("functions.php");

	if(isset($_POST["remove_member"])){
		//echo "You want to delete a member ".$_POST['remove_member'];
		require_once("data/member_dal.php");
		$member_id = intval($_POST['remove_member']);
		$removed_member = MemberDAL::delete($member_id);
		$json = array();
		if($removed_member){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_other_income_type"])){
		//echo "You want to delete a member ".$_POST['remove_member'];
		require_once("data/other_income_type_dal.php");
		$other_income_type_id = intval($_POST['remove_other_income_type']);
		$removed_other_income_type = OtherIncomeTypeDAL::delete($other_income_type_id);
		$json = array();
		if($removed_other_income_type){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}
	

	if(isset($_POST["remove_other_income"])){
		//echo "You want to delete a member ".$_POST['remove_member'];
		require_once("data/other_income_dal.php");
		$other_income_id = intval($_POST['remove_other_income']);
		$removed_other_income = OtherIncomeDAL::delete($other_income_id);

		$json = array();
		if($removed_other_income){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_tithe"])){
		require_once("data/tithe_dal.php");
		$tithe_id = intval($_POST['remove_tithe']);
		$removed_tithe = TitheDAL::delete($tithe_id);
		$json = array();
		if($removed_tithe){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	
	if(isset($_POST["remove_fund_raising_type"])){
		require_once("data/fund_raising_type_dal.php");
		$fund_raising_type_id = intval($_POST['remove_fund_raising_type']);
		$removed_fund_raising_type = FundraisingTypeDAL::delete($fund_raising_type_id);

		$json = array();
		if($removed_fund_raising_type){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}
	if(isset($_POST["remove_fund_raising"])){
		require_once("data/fund_raising_dal.php");
		$fund_raising_id = intval($_POST['remove_fund_raising']);
		$removed_fund_raising = FundraisingDAL::delete($fund_raising_id);

		$json = array();
		if($removed_fund_raising){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_redeemed_fund_raising"])){
		require_once("data/redeemed_pledge_dal.php");
		$redeemed_fund_raising_id = intval($_POST['remove_redeemed_fund_raising']);
		$removed_redeemed_fund_raising = RedeemedPledgeDAL::delete($redeemed_fund_raising_id);

		$json = array();
		if($removed_redeemed_fund_raising){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}


	if(isset($_POST["remove_expenditure_account"])){
		require_once("data/expenditure_account_dal.php");
		$expenditure_account_id = intval($_POST['remove_expenditure_account']);
		$removed_expenditure_account = ExpenditureAccountDAL::delete($expenditure_account_id);

		$json = array();
		if($removed_expenditure_account){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_fellowship"])){
		require_once("data/fellowship_dal.php");
		$fellowship_id = intval($_POST['remove_fellowship']);
		$removed_fellowship = FellowshipDAL::delete($fellowship_id);
		$json = array();
		if($removed_fellowship){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_department"])){
		require_once("data/department_dal.php");
		$department_id = intval($_POST['remove_department']);
		$removed_department = DepartmentDAL::delete($department_id);
		$json = array();
		if($removed_department){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_offering_type"])){
		require_once("data/offering_type_dal.php");
		$offering_type_id = intval($_POST['remove_offering_type']);
		$removed_offering_type = OfferingTypeDAL::delete($offering_type_id);
		$json = array();
		if($removed_offering_type){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_offering"])){
		require_once("data/offering_dal.php");
		$offering_id = intval($_POST['remove_offering']);
		$removed_offering = OfferingDAL::delete($offering_id);
		$json = array();
		if($removed_offering){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}



	if(isset($_POST["remove_group"])){
		require_once("data/group_dal.php");
		$group_id = intval($_POST['remove_group']);
		$removed_group= GroupDAL::delete($group_id);
		$json = array();
		if($removed_group){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_assembly"])){
		require_once("data/assembly_dal.php");
		$assembly_id = intval($_POST['remove_assembly']);
		$removed_assembly = AssemblyDAL::delete($assembly_id);
		$json = array();
		if($removed_assembly){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}

	if(isset($_POST["remove_expenditure"])){
		require_once("data/expenditure_dal.php");
		$expenditure_id = intval($_POST['remove_expenditure']);
		$removed_expenditure = ExpenditureDAL::delete($expenditure_id);
		$json = array();
		if($removed_expenditure){
			$json['status'] = true;
		}else {
			$json['status'] = false;
		}

		echo json_encode($json);
		
	}


	if(isset($_POST["state_id"])){
		$state_id = strip_tags($_POST["state_id"]);

		//Get id and name FROM lgas Where state_id is given
		require_once("data/lga_dal.php");
		$data = LGADAL::fetch_all($state_id);
		$list = array();

		//build JSON
		foreach($data as $content){
			$list[] = array("id" => $content->id, "name" => $content->name);
		
		}

		echo json_encode($list);
		
	}

	if(isset($_POST["card_no"])){
		$card_number = strip_tags($_POST["card_no"]);

		//get_tithe_details_json($card_number);
		require_once("data/member_dal.php");

		$status = MemberDAL::fetchByCardNumber($card_number);

		if(is_bool($status) && !$status){
			$state = "failure";
			$json = array(
				"status" => $state
			);

		}else {
			$state =  "success";
			$json = array(
				"status" => $state,
				"id" => $status->id,
				"full_name" => $status->last_name.", ".$status->first_name." ".$status->middle_name,
				"img_url" => $status->avatar_location
			);
		}
		echo json_encode($json);
	}

	if(isset($_POST["tithe_card_no"])){
		//Query the database and see if the tithe_card_no exists already 
		//If it exist already: Another user is making use of that number 
			//--> Show a visual that the card_no is in use
		//
		require_once("data/member_dal.php");
		$card_number = strip_tags($_POST['tithe_card_no']);
		$data = MemberDAL::fetchByCardNumber($card_number);
		$json = array();

		if(empty($data)){
			$json["status"] = "success";
		}else {
			$json["status"] = "failure";
		}

		echo json_encode($json);

	}