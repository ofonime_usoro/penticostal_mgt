<?php
    session_start();
    require_once("data/fund_raising_type.php");
    require_once("data/fund_raising_type_dal.php");


    $status = array();
    $fund_raising_type = null;

    if(! isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }



    if(isset($_POST['submit']) || isset($_POST['update'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName']) && isset($_POST['txtAmountRealized']) && !empty($_POST['txtAmountRealized']) && isset($_POST['txtDate']) && !empty($_POST['txtDate'])){
            $name = strip_tags($_POST['txtName']);
            $amountRealized = strip_tags($_POST['txtAmountRealized']);
            $date = strip_tags($_POST['txtDate']);
            $description = strip_tags($_POST['txtDescription']);

            $fund_raising_type = new FundRaisingType();

            $fund_raising_type->set_name($name);
            $fund_raising_type->set_amount_realized($amountRealized);
            //$date = explode("/", $date);
            //$date = $date[2]. "-".$date[1]."-".$date[0];
            $fund_raising_type->set_date($date);
            $fund_raising_type->set_date_created(date('Y-m-d H:i:s'));
            $fund_raising_type->set_user_id($_SESSION['user_id']);
            $fund_raising_type->set_description($description);

            $fund_raising_type_model = new FundRaisingTypeDAL($fund_raising_type);

            if(isset($_POST['submit'])){
                $flag = $fund_raising_type_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your fund raising type was saved successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting funds. Try again';
                }
            }else {
                $fund_raising_type->set_id($_GET['fund_raising_type_id']);
                $flag = $fund_raising_type_model->update();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Your fund was updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Inserting Fund Raising Type. Try again';
                }
            }
        }
    }

    if(isset($_GET['fund_raising_type_id'])){
        $fund_raising_type = FundRaisingTypeDAL::fetch($_GET['fund_raising_type_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(5, 0);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Fund Raising Type</h1>
                        <h4>Form Fund Raising Type sample</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Fund Raising Type</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Fund Raising Type Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="#" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="txtName">Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtName" id="txtName" class="input-xlarge" data-rule-required="true" 
                                                value="<?php echo ($fund_raising_type != null) ? $fund_raising_type->get_name() : ''; ?>" 
                                                data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="txtAmountRealized">Amount Realized</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="txtAmountRealized" id="txtAmountRealized" class="input-xlarge"
                                                value="<?php echo ($fund_raising_type != null) ? $fund_raising_type->get_amount_realized() : ''; ?>"
                                                data-rule-required="true" data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>
                                     <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" placeholder="Enter Description"><?php echo ($fund_raising_type != null) ? $fund_raising_type->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" readonly size="16" type="text" name="txtDate" value="<?php echo ($fund_raising_type != null) ? $fund_raising_type->get_date() : date('d/m/Y', time()) ?>">
                                         </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['fund_raising_type_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                                        name="<?php if(isset($_GET['fund_raising_type_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                
                <?php include('footer.php');?>