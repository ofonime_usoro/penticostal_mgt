<?php
    session_start(); 
    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }
    
    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(7, 2);

?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Form Other Income Account</h1>
                        <h4>Form Other Income Account</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Form Other Income Account</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Other Income Account Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="#" class="form-horizontal" id="validation-form" method="get">
                                    <div class="control-group">
                                        <label class="control-label" for="type">Category Type</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="type" id="type" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="type">Name</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <input type="text" name="name" id="name" class="input-xlarge" data-rule-required="true" data-rule-minlength="3" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="PermanentAddress" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="permanentaddress" placeholder="Enter Description"></textarea>
                                        </div>
                                    </div> 
                                    <div class="control-group">
                                        <label for="datefield" class="control-label">Date</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="YYYY-MM-DD" name="datefield" id="datefield" data-rule-dateISO="true" data-rule-required="true">
                                        </div>
                                    </div>
                                    
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>