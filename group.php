<?php
    session_start();
    require_once("data/group.php");
    require_once("data/group_dal.php");

    $status = array();
    $group = null;

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
        exit();
    }

    

    if(isset($_POST['update']) || isset($_POST['submit'])){
        if(isset($_POST['txtName']) && !empty($_POST['txtName'])){
            $name = strip_tags($_POST['txtName']);
            $description = strip_tags($_POST['txtDescription']);

            $group = new Group();
            $group->set_name($name);
            $group->set_description($description);

            $group_model = new GroupDAL($group);

            if(isset($_POST['submit'])){

                $flag = $group_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Group was registered successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering group. Try again';
                }

            }else {
                $group->set_id($_GET['group_id']);
                $flag = $group_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Group updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating group. Try again';
                }  
            }
        }
    }

    if(isset($_GET['group_id'])){
        $group = GroupDAL::fetch(intval($_GET['group_id']));
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(1, 4);
?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i> Group</h1>
                        <h4>View of all the group of the penticostal</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Group</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-table"></i> Group</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="POST">
                                    <div class="control-group">
                                        <label for="txtName" class="control-label">Name</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" name="txtName" id="txtName" data-rule-maxlength="50" data-rule-required="true" 
                                            value="<?php echo ($group != null) ? $group->get_name() : ''; ?>" 
                                            >
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" id="txtDescription" name="txtDescription" placeholder="Enter Description"><?php echo ($group != null) ? $group->get_description() : ''; ?></textarea>
                                        </div>
                                    </div>     
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['group_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['group_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>