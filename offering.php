<?php
    session_start();
    require_once("data/offering.php");
    require_once("data/offering_dal.php");
    require_once("data/offering_type_dal.php");
    require_once("functions.php");

    if( ! isset( $_SESSION['user_id'] ) ){
        header("Location: login.php");
        exit();
    }

    $status = array();
    $offering = null;
    //fetch all the available offering_type to populate the dropdown


    if(isset($_POST["submit"]) || isset($_POST['update'])){
        if(isset($_POST['txtAmount']) && !empty($_POST['txtAmount']) && isset($_POST['downOfferingType']) && !empty($_POST['downOfferingType']) && isset($_POST['txtDate']) && !empty($_POST['txtDate'])){
            $offering_amount = strip_tags($_POST['txtAmount']);
            $offering_date = strip_tags($_POST['txtDate']);
            $offering_drop_down = strip_tags($_POST['downOfferingType']);
            $offering_description = strip_tags($_POST['txtDescription']);

            $offering = new Offering();
            $offering->set_type_id(intval($_POST['downOfferingType']));
            $offering->set_user_id($_SESSION['user_id']);
            $offering->set_amount($offering_amount);
            //$offering_date = explode("/", $offering_date);
            //$offering_date = $offering_date[2]. "-".$offering_date[1]."-".$offering_date[0];
            //var_dump($offering_date);
            $offering->set_date($offering_date);

            $offering->set_date_created(date('d/m/Y'));
            $offering->set_description($offering_description);

            $offering_model = new OfferingDAL($offering);

            if(isset($_POST['submit'])){

                $flag = $offering_model->insert();

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Offering was added successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error Registering Offering. Try again';
                }

            }else {
                $offering->set_id($_GET['offering_id']);
                $flag = $offering_model->update(); 

                if($flag == 1)
                {
                    $status['style'] = 'alert-success';
                    $status['title'] = 'Success';
                    $status['message'] = 'Offering updated successfully!';
                }
                else if($flag == 0)
                {
                    $status['style'] = 'alert-error';
                    $status['title'] = 'Error';
                    $status['message'] = 'Error updating offering. Try again';
                }  
            }

        }
    }

    if(isset($_GET['offering_id'])){
        $offering = OfferingDAL::fetch($_GET['offering_id']);
    }

    header('Content-Type: text/html');
    $page_title = 'Dashboard';
    include('header.php');
    include('menu.php');
    
    display_menu(3, 1);

?>
                </ul>
                <!-- END Navlist -->

                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-desktop">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="icon-file-alt"></i>Offering Form</h1>
                        <h4>Use this form to register new Offering</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a>
                            <span class="divider"><i class="icon-angle-right"></i></span>
                        </li>
                        <li class="active">Offering Form</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->

                <?php if(count($status)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="alert <?php echo $status['style']?>">
                        <button class="close" data-dismiss="alert">×</button>
                        <h4> <?php echo $status['title']?></h4>
                        <p> <?php echo $status['message']?></p>
                        </div>
                    </div>  
                 </div>
                 <?php }?>

                <!-- BEGIN Main Content -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="icon-reorder"></i> Offering Form</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <form action="" class="form-horizontal" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="type">Type Id</label>
                                        <div class="controls">
                                            <div class="span12">
                                                <select name='downOfferingType'>

                                                <?php 
                                                    $type_id = ($offering != null) ? $offering->get_type_id() : '0';
                                                    $offering_types = OfferingTypeDAL::fetch_all();

                                                    foreach($offering_types as $offering_type){
                                                        echo '<option value="'.$offering_type->id.'" ';
                                                        echo ($type_id == $offering_type->id) ? 'selected' : '';
                                                        echo '>'.$offering_type->name.'</option>';
                                                    }
                                                    
                                                ?>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtAmount" class="control-label">Amount</label>
                                        <div class="controls">
                                            <input type="text" class="input-xlarge" placeholder="Only digits" name="txtAmount" id="txtAmount" data-rule-digits="true" data-rule-required="true" 
                                            value="<?php echo ($offering != null) ? $offering->get_amount() : ''; ?>"
                                          >
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDate" class="control-label">Date</label>
                                        <div class="controls">
                                            <div class="date date-picker" data-date="12-02-2012" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                                            <input class="date-picker" id="txtDate" readonly size="16" type="text" name="txtDate" value="<?php echo ($offering != null) ? $offering->get_date() : date('d/m/Y', time()); ?>">
                                            </div>
                                         
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label for="txtDescription" class="control-label">Description</label>
                                        <div class="controls">
                        				<textarea class="input-xlarge" name="txtDescription" placeholder="Enter Description"><?php echo ($offering != null) ? $offering->get_description() : ''; ?>              
                                        </textarea>
                                        </div>
                                    </div>   
                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-primary" value='<?php if(isset($_GET['offering_id'])){ echo 'Update'; }else { echo 'Register'; } ?>' 
                    name="<?php if(isset($_GET['offering_id'])) { echo 'update'; }else { echo 'submit'; } ?>">
                                        <button type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Main Content -->
                <?php include('footer.php');?>
                